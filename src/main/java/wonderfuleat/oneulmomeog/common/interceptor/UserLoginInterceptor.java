package wonderfuleat.oneulmomeog.common.interceptor;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import wonderfuleat.oneulmomeog.auth.jwt.JwtUtil;
import wonderfuleat.oneulmomeog.user.domain.User;
import wonderfuleat.oneulmomeog.user.repository.UserRepository;

import javax.naming.NoPermissionException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Component
public class UserLoginInterceptor implements HandlerInterceptor {

    private final JwtUtil jwtUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("[Interceptor] UserLoginInterceptor");

        if (CorsUtils.isPreFlightRequest(request)) {
            return true;
        }
        if (isPreflightRequest(request)) {
            return true;
        }

        // TODO: 2022/11/17 jwt 토큰이 오지 않은 경우 NullPointerException 발생 부분을 처리해 주어야 한다.
        String bearerJwt = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("request = {}", request.getHeaderNames());
        log.info("BearerJwt = {}", bearerJwt);



        String jwt = jwtUtil.getToken(bearerJwt);
        log.info("getToken = {}", jwt);

        Claims parseJwt = jwtUtil.parseJwt(jwt);
        log.info("parseJwt = {}", parseJwt);

        String userId = String.valueOf(parseJwt.get("userId"));
        String nickname = String.valueOf(parseJwt.get("nickname"));
        String zipcode = String.valueOf(parseJwt.get("zipcode"));

        request.setAttribute("id", userId);
        request.setAttribute("nickname", nickname);
        request.setAttribute("zipcode", zipcode);

//        request.setAttribute("id", 1);
//        request.setAttribute("nickname", "user1");
//        request.setAttribute("zipcode", "10323");
        return Boolean.TRUE;
    }

    private boolean isPreflightRequest(HttpServletRequest request) {
        return isOptions(request) && hasHeaders(request) && hasMethod(request) && hasOrigin(request);
    }

    private boolean isOptions(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase(HttpMethod.OPTIONS.toString());
    }

    private boolean hasHeaders(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Headers"));
    }

    private boolean hasMethod(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Method"));
    }

    private boolean hasOrigin(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Origin"));
    }

}
