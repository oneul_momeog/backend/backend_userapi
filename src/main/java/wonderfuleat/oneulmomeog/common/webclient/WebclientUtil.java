package wonderfuleat.oneulmomeog.common.webclient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class WebclientUtil {

    // webclient 생성 메서드
    public WebClient makeWebClient(String baseUrl, String contentType) {
        return WebClient.builder()
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, contentType)
                .build();
    }
}
