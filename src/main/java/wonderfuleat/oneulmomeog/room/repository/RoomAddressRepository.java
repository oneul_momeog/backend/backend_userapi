package wonderfuleat.oneulmomeog.room.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.room.domain.RoomAddress;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class RoomAddressRepository {

    private final EntityManager em;

    public RoomAddress save(RoomAddress roomAddress) {
        em.persist(roomAddress);
        return roomAddress;
    }
}
