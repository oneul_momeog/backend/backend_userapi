package wonderfuleat.oneulmomeog.room.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.domain.Room;
import wonderfuleat.oneulmomeog.user.domain.User;

import javax.persistence.EntityManager;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class OrdersRepository {

    private final EntityManager em;
    private final RoomRepository roomRepository;

    /**
     * 주문을 추가하는 메서드
     */
    public Orders addOrder(Long userId, Long restaurantId, Long roomId, String menus, int totalPrice) {
        User user = em.getReference(User.class, userId);
        Room room = roomRepository.findRoomByIdWithAlone(roomId, restaurantId);

        if (Objects.isNull(room)) throw new IllegalArgumentException("해당하는 주문 방이 없습니다.");

        Orders orders = new Orders(user, room, menus, totalPrice);
        em.persist(orders);

        return orders;
    }

    /**
     * 주문을 찾는 메서드
     */
    public Orders findById(Long orderId, Long roomId, Long restaurantId) {
        Orders orders = em.createQuery("select o from Orders o" +
                        " join fetch o.room r" +
                        " join fetch r.restaurant re" +
                        " join fetch re.restaurantAddress" +
                        " where o.id = :orderId and r.id = :roomId and r.restaurant.id = :restaurantId", Orders.class)
                .setParameter("orderId", orderId)
                .setParameter("roomId", roomId)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return orders;
    }

    /**
     * userId, roomId 를 통해 orderId를 찾는 메서드
     */
    public Orders findByUserIdAndRoomID(Long userId, Long roomId) {
        Orders orders = em.createQuery("select o from Orders o" +
                        " where o.user.id = :userId and o.room.id = :roomId", Orders.class)
                .setParameter("userId", userId)
                .setParameter("roomId", roomId)
                .getResultStream()
                .findFirst()
                .orElse(null);
        return orders;
    }
}
















