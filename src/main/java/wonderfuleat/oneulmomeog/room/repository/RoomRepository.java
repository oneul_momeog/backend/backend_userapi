package wonderfuleat.oneulmomeog.room.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.room.domain.Room;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class RoomRepository {

    private final EntityManager em;

    public Room save(Room room) {
        em.persist(room);
        return room;
    }

    public Room findRoomById(Long roomId, Long restaurantId) {
        Room room = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress" +
                        " join fetch r.restaurant re" +
                        " where r.id = :roomId and re.id = :restaurantId and r.status = 'CREATE'" +
                        " and r.maxPeople <> 1", Room.class)
                .setParameter("roomId", roomId)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return room;
    }

    public Room findRoomByIdWithReady(Long roomId, Long restaurantId) {
        Room room = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress" +
                        " join fetch r.restaurant re" +
                        " where r.id = :roomId and re.id = :restaurantId and r.status = 'READY'", Room.class)
                .setParameter("roomId", roomId)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return room;
    }

//    public Room findRoomById(Long roomId, Long restaurantId) {
//        Room singleResult = em.createQuery("select r from Room r" +
//                        " join fetch r.roomAddress" +
//                        " join fetch r.restaurant re" +
//                        " where r.id = :roomId and re.id = :restaurantId" +
//                        " and r.status = 'CREATE'", Room.class)
//                .setParameter("roomId", roomId)
//                .setParameter("restaurantId", restaurantId)
//                .getSingleResult();
//        return singleResult;
//    }

    public Room findRoomByIdWithAlone(Long roomId, Long restaurantId) {
        Room room = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress" +
                        " join fetch r.restaurant re" +
                        " where r.id = :roomId and re.id = :restaurantId and r.status = 'CREATE'", Room.class)
                .setParameter("roomId", roomId)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return room;
    }

    public List<Room> findRoomByRestaurantId(Long restaurantId, Integer offset, Integer limit) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress" +
                        " join fetch r.restaurant re" +
                        " where r.restaurant.id = :restaurantId and r.status = 'CREATE'", Room.class)
                .setParameter("restaurantId", restaurantId)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return rooms;
    }

    public List<Room> findRoomsByZipcode(String zipcode, Integer offset, Integer limit) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress a" +
                        " join r.restaurant" +
                        " where a.zipcode like :zipcode and r.status = 'CREATE'" +
                        " order by r.createdAt", Room.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + '%')
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return rooms;
    }

    public List<Room> findRoomsByZipcodeWithCategory(String zipcode, Integer offset, Integer limit, List<Long> categories) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress a" +
                        " join fetch r.restaurant re" +
                        " where a.zipcode like :zipcode and r.status = 'CREATE'" +
                        " and re.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryId in :categories)" +
                        " order by r.createdAt", Room.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("categories", categories)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return rooms;
    }

    public List<Room> searchRooms(String zipcode, Integer offset, Integer limit, String search) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress ra" +
                        " join fetch r.restaurant rs" +
                        " where ra.zipcode like :zipcode and r.status = 'CREATE'" +
                        " and r.maxPeople <> 1" +
                        " and (rs.restaurantName like :search or r.roomName like :search" +
                        " or rs.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryName like :search))", Room.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("search", "%" + search + "%")
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return rooms;
    }

    public List<Room> searchRooms(String zipcode, Integer offset, Integer limit, String search, List<Long> categories) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress ra" +
                        " join fetch r.restaurant rs" +
                        " where ra.zipcode like :zipcode and r.status = 'CREATE'" +
                        " and r.maxPeople <> 1" +
                        " and (rs.restaurantName like :search or r.roomName like :search" +
                        " or rs.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryName like :search))" +
                        " and rs.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryId in :categories)", Room.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("search", "%" + search + "%")
                .setParameter("categories", categories)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return rooms;
    }

}
