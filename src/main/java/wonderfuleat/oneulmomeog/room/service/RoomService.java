package wonderfuleat.oneulmomeog.room.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.room.domain.Room;
import wonderfuleat.oneulmomeog.room.dto.RoomDataDto;
import wonderfuleat.oneulmomeog.room.repository.RoomRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class RoomService {

    private final RoomRepository roomRepository;

    /**
     * 마감 임박 방 목록
     */
    public List<RoomDataDto> getRooms(String zipcode) {
        log.info("getRooms with no categories");
        List<Room> roomsByZipcode = roomRepository.findRoomsByZipcode(zipcode, 0, 12);
        log.info("sql result = {}", roomsByZipcode.toString());
        List<RoomDataDto> data = changeRoomToDto(roomsByZipcode);

        return data;
    }

    /**
     * 모집중인 공동구매 목록
     */
    public List<RoomDataDto> getRooms(String zipcode, Integer offset, Integer limit, List<Long> categories) {
        log.info("getRooms with categories");
        List<Room> roomsByZipcodeWithCategory = roomRepository.findRoomsByZipcodeWithCategory(zipcode, offset, limit, categories);
        List<RoomDataDto> data = changeRoomToDto(roomsByZipcodeWithCategory);

        return data;
    }

    /**
     * 카테고리 없이 룸 검색
     */
    public List<RoomDataDto> searchRooms(String zipcode, Integer offset, Integer limit, String search) {
        log.info("searchRooms with no categories");
        List<Room> rooms = roomRepository.searchRooms(zipcode, offset, limit, search);
        List<RoomDataDto> data = changeRoomToDto(rooms);

        return data;
    }

    public List<RoomDataDto> searchRooms(String zipcode, Integer offset, Integer limit, String search, List<Long> categories) {
        log.info("searchRooms with categories");
        List<Room> rooms = roomRepository.searchRooms(zipcode, offset, limit, search, categories);
        List<RoomDataDto> data = changeRoomToDto(rooms);

        return data;
    }

    /**
     * roomId를 이용해 room 정보를 찾아주는 메서드
     */
    public RoomDataDto getRoomInfo(Long roomId, Long restaurantId) {

        Room room = roomRepository.findRoomById(roomId, restaurantId);

        if (Objects.isNull(room)) throw new IllegalArgumentException("해당 공동구매 룸이 없습니다.");

        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime dueTime = room.getCreatedAt().plusMinutes(room.getTimer());
        log.info("currentTime = {}, dueTime = {}", currentTime, dueTime);

        RoomDataDto data = new RoomDataDto(
                room.getId(),
                room.getRestaurant().getId(),
                room.getRoomName(),
                room.getRestaurant().getRestaurantImage(),
                room.getRestaurant().getRestaurantName(),
                room.getMaxPeople(),
                room.getCurrentPeople(),
                room.getRoomAddress().getNormalAddress(),
                room.getRoomAddress().getSpecificAddress(),
                currentTime,
                dueTime
        );
        return data;
    }

    /**
     * restaurantId를 이용하여 room 목록을 찾아주는 메서드
     */
    public List<RoomDataDto> getRoomListByRestaurantId(Long restaurantId, Integer offset, Integer limit) {
        log.info("get room list By restaurantId");
        List<Room> rooms = roomRepository.findRoomByRestaurantId(restaurantId, offset, limit);

        List<RoomDataDto> data = changeRoomToDto(rooms);
        return data;
    }




    // room 리스트들을 dto로 변환해주는 메서드
    private List<RoomDataDto> changeRoomToDto(List<Room> roomsByZipcode) {
        List<RoomDataDto> data = new ArrayList<>();

        roomsByZipcode.stream().forEach(room -> {
            String normalAddress = Objects.isNull(room.getRoomAddress().getNormalAddress()) ? null : room.getRoomAddress().getNormalAddress();
            String specificAddress = Objects.isNull(room.getRoomAddress().getSpecificAddress()) ? null : room.getRoomAddress().getSpecificAddress();
            LocalDateTime currentTime = LocalDateTime.now();
            LocalDateTime dueTime = room.getCreatedAt().plusMinutes(room.getTimer());
            log.info("currentTime = {}, dueTime = {}", currentTime, dueTime);

            RoomDataDto roomLastDataDto = new RoomDataDto(
                    room.getId(), room.getRestaurant().getId(), room.getRoomName(),
                    room.getRestaurant().getRestaurantImage(), room.getRestaurant().getRestaurantName(), room.getMaxPeople(),
                    room.getCurrentPeople(), normalAddress, specificAddress, currentTime, dueTime);
            data.add(roomLastDataDto);
        });
        return data;
    }


}
