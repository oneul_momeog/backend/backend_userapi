package wonderfuleat.oneulmomeog.room.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import wonderfuleat.oneulmomeog.SimpleTransactionService;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.common.webclient.WebclientUtil;
import wonderfuleat.oneulmomeog.food.domain.Menu;
import wonderfuleat.oneulmomeog.food.repository.MenuRepository;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.domain.Room;
import wonderfuleat.oneulmomeog.room.domain.RoomAddress;
import wonderfuleat.oneulmomeog.room.domain.RoomStatus;
import wonderfuleat.oneulmomeog.room.dto.*;
import wonderfuleat.oneulmomeog.room.repository.OrdersRepository;
import wonderfuleat.oneulmomeog.room.repository.RoomAddressRepository;
import wonderfuleat.oneulmomeog.room.repository.RoomRepository;
import wonderfuleat.oneulmomeog.timer.ScheduledJob;
import wonderfuleat.oneulmomeog.user.domain.UserAddress;
import wonderfuleat.oneulmomeog.user.repository.UserAddressRepository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class OrdersService {

    private final OrdersRepository ordersRepository;
    private final MenuRepository menuRepository;
    private final RoomRepository roomRepository;
    private final EntityManager em;
    private final UserAddressRepository userAddressRepository;
    private final RoomAddressRepository roomAddressRepository;
    private final SimpleTransactionService simpleTransactionService;
    private final WebclientUtil webclientUtil;


    private ObjectMapper objectMapper = new ObjectMapper();

    // TODO: 2022/11/07 서비스 공통 부분 리팩토링 필요 PayService 부분도 마찬가지로 리팩토링 필요
    /**
     * 방에 참가하여 구매하는 경우
     */
    @Transactional
    public OrderDataDto orderWithRoom(Long userId, Long restaurantId, Long roomId, List<OrderMenusDto> menus) throws JsonProcessingException {

        // menuId들을 추출하여 해당 메뉴를 가져온다.
        List<Menu> menuList = getMenus(menus);

        // 가져온 메뉴를 이용하여 전달할 데이터 형태와 totalPrice 값을 계산한다.
        AtomicInteger totalPrice = new AtomicInteger();
        List<MenuDataDto> menuDataDtoList = new ArrayList<>();

        menuList.stream().forEach(menu -> {
            for (OrderMenusDto menuDto : menus) {
                if (menu.getId() == menuDto.getMenuId()) {
                    totalPrice.addAndGet(menu.getPrice() * menuDto.getCount());
                    MenuDataDto menuDataDto = new MenuDataDto(
                            menu.getId(),
                            menu.getMenuName(),
                            menu.getPrice(),
                            menuDto.getCount()
                    );

                    menuDataDtoList.add(menuDataDto);
                    break;
                }
            }
        });

        // 메뉴들을 DB에 저장할 수 있는 형태로 변경해야한다.
        String menusToString = objectMapper.writeValueAsString(menus);

        Orders orders = ordersRepository.addOrder(userId, restaurantId, roomId, menusToString, totalPrice.get());
        log.info("save orders : orderId = {}", orders.getId());

        // room의 참가 인원과 총 가격을 변경해 주어야 한다.
        orders.getRoom().plusCurrentPeople();
        orders.getRoom().updateTotalPrice(orders.getTotalPrice());

        return new OrderDataDto(
                orders.getId(),
                orders.getRoom().getId(),
                orders.getRoom().getRestaurant().getRestaurantName(),
                orders.getRoom().getRoomAddress().getNormalAddress() + " " + orders.getRoom().getRoomAddress().getSpecificAddress(),
                menuDataDtoList,
                totalPrice.get(),
                orders.getRoom().getRestaurant().getDeliveryFee());
    }

    /**
     * 혼자 구매하는 경우
     */
    @Transactional
    public OrderDataDto orderWithSingle(Long userId, Long restaurantId, List<OrderMenusDto> menus) throws JsonProcessingException {
        List<Menu> menuList = getMenus(menus);

        log.info("menuList = {}", menuList.toString());

        // 가져온 메뉴를 이용하여 전달할 데이터 형태와 totalPrice 값을 계산한다.
        AtomicInteger totalPrice = new AtomicInteger();
        List<MenuDataDto> menuDataDtoList = new ArrayList<>();

        menuList.stream().forEach(menu -> {
            for (OrderMenusDto menuDto : menus) {
                if (menu.getId() == menuDto.getMenuId()) {
                    totalPrice.addAndGet(menu.getPrice() * menuDto.getCount());
                    MenuDataDto menuDataDto = new MenuDataDto(
                            menu.getId(),
                            menu.getMenuName(),
                            menu.getPrice(),
                            menuDto.getCount()
                    );

                    menuDataDtoList.add(menuDataDto);
                    break;
                }
            }
        });

        // 혼자 구매하는 방 생성
        Restaurant restaurant = em.getReference(Restaurant.class, restaurantId);
        // 유저 정보를 이용하여 유저의 주소를 가져오고 이를 룸 주소로 저장해준다.
        UserAddress userAddress = userAddressRepository.findByUserId(userId);
        RoomAddress roomAddress = new RoomAddress(userAddress.getZipcode(), userAddress.getNormalAddress(), userAddress.getSpecificAddress());

        Room room = new Room(restaurant, roomAddress, "혼자 구매", menuList.get(0).getMenuName() + "...", 1, 1, totalPrice.get(), 0, RoomStatus.CREATE);

        log.info("room = {}", room.getRoomName());

        roomAddressRepository.save(roomAddress);
        roomRepository.save(room);

        // order 테이블에 주문 생성
        // 메뉴들을 DB에 저장할 수 있는 형태로 변경해야한다.
        String menusToString = objectMapper.writeValueAsString(menus);

        Orders orders = ordersRepository.addOrder(userId, restaurantId, room.getId(), menusToString, totalPrice.get());
        log.info("save orders : orderId = {}", orders.getId());

        return new OrderDataDto(
                orders.getId(),
                orders.getRoom().getId(),
                orders.getRoom().getRestaurant().getRestaurantName(),
                orders.getRoom().getRoomAddress().getNormalAddress() + " " + orders.getRoom().getRoomAddress().getSpecificAddress(),
                menuDataDtoList,
                totalPrice.get(),
                orders.getRoom().getRestaurant().getDeliveryFee());
    }

    /**
     * 방만들어서 구매하는 경우
     * timer 설정이 필요하다.
     */
    @Transactional
    public OrderDataDto orderWithCreate(
            Long userId,
            Long restaurantId,
            String roomName,
            String zipcode,
            String normalAddress,
            String specificAddress,
            List<OrderMenusDto> menus,
            Integer maxPeople,
            Integer timer) throws JsonProcessingException {

        List<Menu> menuList = getMenus(menus);
        log.info("menuList = {}", menuList.toString());

        // 가져온 메뉴를 이용하여 전달할 데이터 형태와 totalPrice 값을 계산한다.
        AtomicInteger totalPrice = new AtomicInteger();
        List<MenuDataDto> menuDataDtoList = new ArrayList<>();

        menuList.stream().forEach(menu -> {
            for (OrderMenusDto menuDto : menus) {
                if (menu.getId() == menuDto.getMenuId()) {
                    totalPrice.addAndGet(menu.getPrice() * menuDto.getCount());
                    MenuDataDto menuDataDto = new MenuDataDto(
                            menu.getId(),
                            menu.getMenuName(),
                            menu.getPrice(),
                            menuDto.getCount()
                    );

                    menuDataDtoList.add(menuDataDto);
                    break;
                }
            }
        });

        // 공동 구매하는 방 생성
        Restaurant restaurant = em.getReference(Restaurant.class, restaurantId);
        RoomAddress roomAddress = new RoomAddress(zipcode, normalAddress, specificAddress);

        Room room = new Room(restaurant, roomAddress, roomName, menuList.get(0).getMenuName() + "...", maxPeople, 1, totalPrice.get(), timer, RoomStatus.CREATE);
        log.info("room = {}", room.getRoomName());

        roomAddressRepository.save(roomAddress);
        roomRepository.save(room);

        // order 테이블에 주문 생성
        // 메뉴들을 DB에 저장할 수 있는 형태로 변경해야한다.
        String menusToString = objectMapper.writeValueAsString(menus);

        Orders orders = ordersRepository.addOrder(userId, restaurantId, room.getId(), menusToString, totalPrice.get());
        log.info("save orders : orderId = {}", orders.getId());

        // timer 설정
        log.info("order class not int timer = {}, time = {}", orders.getClass(), LocalDateTime.now());
        addTimerTask(orders.getId(), room.getId(), restaurantId, timer);


        return new OrderDataDto(
                orders.getId(),
                orders.getRoom().getId(),
                orders.getRoom().getRestaurant().getRestaurantName(),
                orders.getRoom().getRoomAddress().getNormalAddress() + " " + orders.getRoom().getRoomAddress().getSpecificAddress(),
                menuDataDtoList,
                totalPrice.get(),
                orders.getRoom().getRestaurant().getDeliveryFee());
    }

    private void addTimerTask(Long orderId, Long roomId, Long restaurantId, Integer timer) {
        Timer scheduler = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                simpleTransactionService.executeTransaction(() -> {
                    log.info("timer start time = {}", LocalDateTime.now());

                    Orders orders = ordersRepository.findById(orderId, roomId, restaurantId);
                    orders.getRoom().updateCreateToReady();
                    // ready로 바꾸고 소켓서버에 알려줘야 한다.
                    WebClient webClient = webclientUtil.makeWebClient(
                            "http://springboot-websocket-svc:8081",
                            MediaType.APPLICATION_JSON_VALUE);

                    // 룸정보를 직접 보내주자
                    Room room = orders.getRoom();
                    log.info("room = {}", room);
                    if (Objects.isNull(room)) throw new IllegalArgumentException("잘못된 룸 정보입니다.");

                    RoomReadyRequest roomReadyRequest = new RoomReadyRequest(room.getRestaurant().getId(), room.getId(), room.getExMenu(), room.getTotalPrice(),
                            room.getCreatedAt().plusMinutes(room.getTimer()),
                            room.getRoomAddress().getNormalAddress() + " " + room.getRoomAddress().getSpecificAddress(),
                            room.getStatus()
                    );

                    objectMapper.registerModule(new JavaTimeModule());
                    RoomReadyRequest data = objectMapper.convertValue(roomReadyRequest, new TypeReference<>() {});
                    log.info("request data = {}", data);

                    Mono<SuccessResult> response = webClient.post()
                            .uri("/api/ceo/sse/room-ready")
                            .bodyValue(data)
                            .accept(MediaType.APPLICATION_JSON)
                            .retrieve()
                            .bodyToMono(SuccessResult.class);
                    SuccessResult result = response.block();
                    log.info("Mono sse response = {}", result);
                });
            }
        };
        scheduler.schedule(task, timer * 60 * 1000);
//        scheduler.schedule(task, 3 * 1000);

    }


    // 전달받은 메뉴 목록을 이용해 DB에서 메뉴 목록을 가져오는 메서드
    public List<Menu> getMenus(List<OrderMenusDto> menus) {
        List<Long> menuIds = menus.stream()
                .map(menu -> menu.getMenuId())
                .collect(Collectors.toList());
        List<Menu> menuList = menuRepository.getMenus(menuIds);
        return menuList;
    }

}
