package wonderfuleat.oneulmomeog.room.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import wonderfuleat.oneulmomeog.SimpleTransactionService;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.common.webclient.WebclientUtil;
import wonderfuleat.oneulmomeog.food.domain.Menu;
import wonderfuleat.oneulmomeog.food.repository.MenuRepository;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.domain.Room;
import wonderfuleat.oneulmomeog.room.domain.RoomAddress;
import wonderfuleat.oneulmomeog.room.domain.RoomStatus;
import wonderfuleat.oneulmomeog.room.dto.MenuDataDto;
import wonderfuleat.oneulmomeog.room.dto.OrderMenusDto;
import wonderfuleat.oneulmomeog.room.dto.PayOrderDataDto;
import wonderfuleat.oneulmomeog.room.dto.RoomReadyRequest;
import wonderfuleat.oneulmomeog.room.repository.OrdersRepository;
import wonderfuleat.oneulmomeog.room.repository.RoomRepository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class PayService {

    private final RoomRepository roomRepository;
    private final OrdersRepository ordersRepository;
    private final OrdersService ordersService;
    private final SimpleTransactionService simpleTransactionService;
    private final WebclientUtil webclientUtil;

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 룸 주소를 변경하는 서비스
     */
    public void changeRoomAddress(Long roomId, Long restaurantId, String zipcode, String normalAddress, String specificAddress) {
        Room room = roomRepository.findRoomById(roomId, restaurantId);
        RoomAddress roomAddress = room.getRoomAddress();

        roomAddress.changeRoomAddress(zipcode, normalAddress, specificAddress);
    }

    /**
     * 주문 결제 서비스
     */
    public PayOrderDataDto payOrder(Long orderId, Long roomId, Long restaurantId, Integer price) throws JsonProcessingException {

        // orders 테이블의 결제상태를 pay로 변경한다. -> 이때 room의 상태가 접수대기인지 확인해야 한다.
        Orders orders = ordersRepository.findById(orderId, roomId, restaurantId);
        if (Objects.isNull(orders)) throw new IllegalArgumentException("해당하는 주문이 존재하지 않습니다.");

        if (!orders.getRoom().getStatus().equals(RoomStatus.CREATE)) {
            throw new IllegalArgumentException("이미 주문 마감된 룸입니다.");
        }
        orders.changeStatusToPay();

        // menuDataDto 생성하기
        List<OrderMenusDto> menus = objectMapper.readValue(orders.getMenus(), new TypeReference<List<OrderMenusDto>>() {
        });
        List<Menu> menuList = ordersService.getMenus(menus);

        log.info("menuList = {}", menuList.toString());

        // 가져온 메뉴를 이용하여 전달할 데이터 형태와 totalPrice 값을 계산한다.
        AtomicInteger totalPrice = new AtomicInteger();
        List<MenuDataDto> menuDataDtoList = new ArrayList<>();

        menuList.stream().forEach(menu -> {
            for (OrderMenusDto menuDto : menus) {
                if (menu.getId() == menuDto.getMenuId()) {
                    totalPrice.addAndGet(menu.getPrice() * menuDto.getCount());
                    MenuDataDto menuDataDto = new MenuDataDto(
                            menu.getId(),
                            menu.getMenuName(),
                            menu.getPrice(),
                            menuDto.getCount()
                    );

                    menuDataDtoList.add(menuDataDto);
                    break;
                }
            }
        });

        if (totalPrice.get() + orders.getRoom().getRestaurant().getDeliveryFee() != price) throw new IllegalArgumentException("총 결제금액이 일치하지 않습니다.");

        // 혼자 구매한 경우 room 의 status 상태를 READY로 변경해야 한다.
        if (orders.getRoom().getMaxPeople() == 1) {
            // ready로 바꾼 후 socket서버에 알려줘야 한다.
//            addTimerTask(orders.getId(), orders.getRoom().getId(), orders.getRoom().getRestaurant().getId(), 0);
            requestSseEmitter(orders.getId(), orders.getRoom().getId(), orders.getRoom().getRestaurant().getId());
        }

        PayOrderDataDto data = new PayOrderDataDto(
                orders.getRoom().getRestaurant().getId(),
                orders.getRoom().getId(),
                orders.getId(),
                orders.getRoom().getRestaurant().getRestaurantName(),
                orders.getPayTime(),
                menuDataDtoList,
                totalPrice.get() + orders.getRoom().getRestaurant().getDeliveryFee(),
                orders.getRoom().getRestaurant().getDeliveryFee()
        );

        return data;
    }

    private void requestSseEmitter(Long orderId, Long roomId, Long restaurantId) {
        Orders orders = ordersRepository.findById(orderId, roomId, restaurantId);
        orders.getRoom().updateCreateToReady();
        // ready로 바꾸고 소켓서버에 알려줘야 한다.
        WebClient webClient = webclientUtil.makeWebClient(
                "http://springboot-websocket-svc:8081",
                MediaType.APPLICATION_JSON_VALUE);

        // 룸정보를 직접 보내주자
        Room room = orders.getRoom();
        log.info("room = {}", room);
        if (Objects.isNull(room)) throw new IllegalArgumentException("잘못된 룸 정보입니다.");

        RoomReadyRequest roomReadyRequest = new RoomReadyRequest(room.getRestaurant().getId(), room.getId(), room.getExMenu(), room.getTotalPrice(),
                room.getCreatedAt().plusMinutes(room.getTimer()),
                room.getRoomAddress().getNormalAddress() + " " + room.getRoomAddress().getSpecificAddress(),
                room.getStatus()
        );

        objectMapper.registerModule(new JavaTimeModule());
        RoomReadyRequest data = objectMapper.convertValue(roomReadyRequest, new TypeReference<>() {});
        log.info("request data = {}", data);

        Mono<SuccessResult> response = webClient.post()
                .uri("/api/ceo/sse/room-ready")
                .bodyValue(data)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(SuccessResult.class);
        SuccessResult result = response.block();
        log.info("Mono sse response = {}", result);
    }
}



























