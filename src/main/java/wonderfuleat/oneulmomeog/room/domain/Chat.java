package wonderfuleat.oneulmomeog.room.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import wonderfuleat.oneulmomeog.common.domain.Generate;

import javax.persistence.*;

@Entity
@DynamicInsert
@Getter
public class Chat extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chat_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    @Column(nullable = false)
    private int userId;

    @Column(nullable = false)
    private String userNickname;

    @Column(nullable = false)
    private String content;

    @ColumnDefault("'text'")
    private String isImage;
}
