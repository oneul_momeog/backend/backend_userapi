package wonderfuleat.oneulmomeog.room.domain;

public enum RoomStatus {
    CREATE, READY, RECEIVE, DELIVERY, FINISH, CANCEL
}
