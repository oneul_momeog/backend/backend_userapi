package wonderfuleat.oneulmomeog.room.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import wonderfuleat.oneulmomeog.common.domain.Generate;
import wonderfuleat.oneulmomeog.user.domain.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@DynamicInsert
@Getter
public class Orders extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String menus;

    @Column(nullable = false)
    private int totalPrice;

    @Enumerated(EnumType.STRING)
    @ColumnDefault("'PUT'")
    private UserRoomStatus status;

    @Column
    private LocalDateTime payTime;

    // 생성자 메서드
    protected Orders() {}

    public Orders(User user, Room room, String menus, int totalPrice) {
        this.user = user;
        this.room = room;
        this.menus = menus;
        this.totalPrice = totalPrice;
    }

    // 비즈니스 메서드
    public void changeStatusToPay() {
        this.status = UserRoomStatus.PAY;
        this.payTime = LocalDateTime.now();
    }
}
