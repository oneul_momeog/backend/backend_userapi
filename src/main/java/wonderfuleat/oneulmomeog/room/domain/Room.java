package wonderfuleat.oneulmomeog.room.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import wonderfuleat.oneulmomeog.common.domain.Generate;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DynamicInsert
@Getter
public class Room extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "room_address_id")
    private RoomAddress roomAddress;

    @Column(nullable = false)
    private String roomName;

    @Column(nullable = false)
    private String exMenu;

    private Integer maxPeople;
    private Integer currentPeople;

    @Column(nullable = false)
    @ColumnDefault("0")
    private int totalPrice;

    private Integer timer;

    @Enumerated(EnumType.STRING)
    @ColumnDefault("'CREATE'")
    private RoomStatus status;

    @OneToMany(mappedBy = "room")
    private List<Orders> orders = new ArrayList<>();


    // 생성자 메서드
    protected Room() {}

    public Room(Restaurant restaurant, RoomAddress roomAddress, String roomName, String exMenu, Integer maxPeople, Integer currentPeople, Integer totalPrice,  Integer timer, RoomStatus roomStatus) {
        this.restaurant = restaurant;
        this.roomAddress = roomAddress;
        this.roomName = roomName;
        this.exMenu = exMenu;
        this.maxPeople = maxPeople;
        this.currentPeople = currentPeople;
        this.totalPrice = totalPrice;
        this.timer = timer;
        this.status = roomStatus;
    }

    /* 비즈니스 메서드 */
    /**
     * 방 참가 인원 업데이트
     */
    public void plusCurrentPeople() {
        if (currentPeople >= maxPeople) {
            throw new IllegalArgumentException("인원이 이미 찼습니다.");
        }
        this.currentPeople += 1;
    }

    /**
     * 총 가격 업데이트
     */
    public void updateTotalPrice(int newPrice) {
        this.totalPrice += newPrice;
    }

    /**
     * 룸 상태 READY로 변경
     */
    // TODO: 2022/11/09 READY로 변경되는 순간 음식점 사장님에게 알람이 가야한다.
    public void updateCreateToReady() {
        this.status = RoomStatus.READY;
    }
}






















