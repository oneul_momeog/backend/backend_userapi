package wonderfuleat.oneulmomeog.room.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.room.dto.PayAddressRequestDto;
import wonderfuleat.oneulmomeog.room.dto.PayOrderDataDto;
import wonderfuleat.oneulmomeog.room.dto.PayOrderRequestDto;
import wonderfuleat.oneulmomeog.room.service.PayService;

import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pay")
@Slf4j
public class PayController {

    private final PayService payService;

    /**
     * 주소 변경 컨트롤러
     */
    @PostMapping("/{restaurantId}/{roomId}/address")
    public SuccessResult editRoomAddress(
            @PathVariable Long restaurantId,
            @PathVariable Long roomId,
            @Validated @RequestBody PayAddressRequestDto body
    ) {
        if (!Objects.equals(restaurantId, body.getRestaurantId())) throw new IllegalArgumentException("음식점 아이디가 일치하지 않습니다.");
        if (!Objects.equals(roomId, body.getRoomId())) throw new IllegalArgumentException("룸 아이디가 일치하지 않습니다.");

        log.info("editRoomAddress controller param : orderId = {}, restaurantId = {}, roomId = {}, zipcode = {}, normalAddress = {}, specificAddress = {}",
                body.getOrderId(), body.getRestaurantId(), body.getRoomId(), body.getZipcode(), body.getNormalAddress(), body.getSpecificAddress()
        );

        payService.changeRoomAddress(body.getRoomId(), body.getRestaurantId(), body.getZipcode(), body.getNormalAddress(), body.getSpecificAddress());

        return new SuccessResult("주소 변경 성공");
    }

    /**
     * 결제하기
     */
    @PostMapping("/{restaurantId}/{roomId}")
    public SuccessResultWithData<PayOrderDataDto> payOrder(
            @PathVariable Long restaurantId,
            @PathVariable Long roomId,
            @RequestBody PayOrderRequestDto body

    ) throws JsonProcessingException {
        if (!Objects.equals(restaurantId, body.getRestaurantId())) throw new IllegalArgumentException("음식점 아이디가 일치하지 않습니다.");
        if (!Objects.equals(roomId, body.getRoomId())) throw new IllegalArgumentException("룸 아이디가 일치하지 않습니다.");

        log.info("payOrder controller param : orderId = {}, restaurantId = {}, roomId = {}, price = {}",
                body.getOrderId(), body.getRestaurantId(), body.getRoomId(), body.getPrice()
        );

        PayOrderDataDto data = payService.payOrder(body.getOrderId(), body.getRoomId(), body.getRestaurantId(), body.getPrice());

        return new SuccessResultWithData<>("결제 성공입니다.", data);
    }
}




























