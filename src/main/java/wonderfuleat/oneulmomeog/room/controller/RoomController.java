package wonderfuleat.oneulmomeog.room.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.room.dto.RoomDataDto;
import wonderfuleat.oneulmomeog.room.dto.RoomListResponseDto;
import wonderfuleat.oneulmomeog.room.service.RoomService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/main")
public class RoomController {

    private final RoomService roomService;

    private final Integer ONE_PAGE_ROOMS_NUM = 6;


    // TODO: 2022/10/30 zipcode를 가져오기 위해 request를 이용하는데 이 부분의 중복을 없애보자.

    /**
     * 마감 임박 가로스크롤
     */
    @GetMapping("/room/last")
    public RoomListResponseDto getLastRooms(HttpServletRequest request) {
        String zipcode = String.valueOf(request.getAttribute("zipcode"));
        log.info("zipcode = {}", zipcode);

        List<RoomDataDto> lastRooms = roomService.getRooms(zipcode);

        RoomListResponseDto response = new RoomListResponseDto("마감 임박 룸 응답 성공", lastRooms);
        return response;
    }

    @GetMapping("/rooms")
    public RoomListResponseDto getAllRooms(
            HttpServletRequest request,
            @RequestParam(value = "category", defaultValue = "0") List<Long> categories,
            @RequestParam(value = "page", defaultValue = "1") Integer page
            ) {

        String zipcode = String.valueOf(request.getAttribute("zipcode"));
        log.info("zipcode = {}", zipcode);
        log.info("categories = {}", categories.toString());
        log.info("page = {}", page);

        int offset = ONE_PAGE_ROOMS_NUM * (page - 1);

        List<RoomDataDto> data;

        if (categories.get(0) == 0) {
            data = roomService.getRooms(zipcode);
        } else {
            data = roomService.getRooms(zipcode, offset, ONE_PAGE_ROOMS_NUM, categories);
        }

        RoomListResponseDto response = new RoomListResponseDto("룸 목록 응답 성공", data);
        return response;
    }

    @GetMapping("/rooms/search")
    public RoomListResponseDto getRoomsBySearch(
            HttpServletRequest request,
            @RequestParam String search,
            @RequestParam(value = "category", defaultValue = "0") List<Long> categories,
            @RequestParam(value = "page", defaultValue = "1") Integer page
    ) {
        String zipcode = String.valueOf(request.getAttribute("zipcode"));
        log.info("param : search = {}, categories = {}", search, categories.toString());

        int offset = ONE_PAGE_ROOMS_NUM * (page - 1);

        List<RoomDataDto> data;
        if (categories.get(0) == 0) {
            data = roomService.searchRooms(zipcode, offset, ONE_PAGE_ROOMS_NUM, search);
        } else {
            data = roomService.searchRooms(zipcode, offset, ONE_PAGE_ROOMS_NUM, search, categories);
        }

        RoomListResponseDto response = new RoomListResponseDto("룸 목록 응답 성공", data);
        return response;
    }
}






















