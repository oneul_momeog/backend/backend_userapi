package wonderfuleat.oneulmomeog.room.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.room.dto.OrderDataDto;
import wonderfuleat.oneulmomeog.room.dto.OrderMenusDto;
import wonderfuleat.oneulmomeog.room.dto.OrderWithCreateRequestDto;
import wonderfuleat.oneulmomeog.room.dto.OrderWithRoomOrSingleRequestDto;
import wonderfuleat.oneulmomeog.room.service.OrdersService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/restaurants")
@Slf4j
public class OrdersController {

    private final OrdersService ordersService;

    // TODO: 2022/11/06 구매 컨트롤러에 음식점 id에 메뉴가 없는 경우 이를 방어하는 방어 코드가 필요하다.
    // TODO: 2022/11/07 주문하기 클릭 때 만약 방의 상태가 접수 대기로 바뀐다면 주문을 못하고 물리 삭제 시킨다.
    /**
     * 공동주문을 통해 구매하기 클릭 -> 기존 방이 존재하기 때문에 orders 테이블에 추가만 시켜주면 된다.
     */
    @PostMapping("/{restaurantId}/room/{roomId}/order")
    public SuccessResultWithData<OrderDataDto> orderMenuWithRoom(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @PathVariable Long roomId,
            @Validated @RequestBody OrderWithRoomOrSingleRequestDto order
    ) throws JsonProcessingException {

        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        if (roomId != order.getRoomId()) throw new IllegalArgumentException("룸 번호가 다릅니다.");
        if (restaurantId != order.getRestaurantId()) throw new IllegalArgumentException("음식점 번호가 다릅니다.");
        List<OrderMenusDto> menus = order.getMenus();

        log.info("orderMenuWithRoom controller param : userId = {}, restaurantId = {}, roomId = {}, menus = {}", userId, restaurantId, roomId, menus.toString());

        OrderDataDto data = ordersService.orderWithRoom(userId, restaurantId, roomId, menus);

        return new SuccessResultWithData<>("방에 참여하여 구매하기 성공", data);
    }

    /**
     * 혼자 주문을 통해 구매하기 클릭 -> 기존 방이 없기 때문에 orders 테이블과 room 테이블 둘 다 추가시켜야 한다.
     */
    @PostMapping("/{restaurantId}/single/order")
    public SuccessResultWithData<OrderDataDto> orderMenuWithSingle(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @Validated @RequestBody OrderWithRoomOrSingleRequestDto order
    ) throws JsonProcessingException {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        if (restaurantId != order.getRestaurantId()) throw new IllegalArgumentException("음식점 번호가 다릅니다.");
        List<OrderMenusDto> menus = order.getMenus();

        log.info("orderMenuWithSingle controller param : userId = {}, restaurantId = {}, menus = {}", userId, restaurantId, menus.toString());

        OrderDataDto data = ordersService.orderWithSingle(userId, restaurantId, menus);

        return new SuccessResultWithData<>("혼자 구매하기 성공", data);

    }

    /**
     * 방 만들어서 구매하기 클릭 -> 기존 방이 없기 때문에 orders 테이블과 room 테이블 둘 다 추가시켜야 한다.
     */
    @PostMapping("/{restaurantId}/create/order")
    public SuccessResultWithData<OrderDataDto> orderMenuWithCreate(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @Validated @RequestBody OrderWithCreateRequestDto order
            ) throws JsonProcessingException {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        if (restaurantId != order.getRestaurantId()) throw new IllegalArgumentException("음식점 번호가 다릅니다.");
        List<OrderMenusDto> menus = order.getMenus();

        log.info("orderMenuWithCreate controller param : userId = {}, restaurantId = {}, menus = {}", userId, restaurantId, menus);

        OrderDataDto data = ordersService.orderWithCreate(userId, restaurantId, order.getRoomName(), order.getZipcode(), order.getNormalAddress(), order.getSpecificAddress(), menus, order.getMaxPeople(), order.getTimer());
        return new SuccessResultWithData<>("방 만들어 구매하기 성공", data);
    }

}


























