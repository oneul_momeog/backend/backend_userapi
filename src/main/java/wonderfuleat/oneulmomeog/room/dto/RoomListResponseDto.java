package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;

import java.util.List;

@Getter
public class RoomListResponseDto extends SuccessResult {
    private List<RoomDataDto> data;

    public RoomListResponseDto(String message, List<RoomDataDto> data) {
        super(message);
        this.data = data;
    }
}
