package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@AllArgsConstructor
public class PayOrderDataDto {

    private Long restaurantId;
    private Long roomId;
    private Long orderId;
    private String restaurantName;
    private LocalDateTime payTime;
    private List<MenuDataDto> menus;
    private Integer totalPrice;
    private Integer deliveryFee;
}
