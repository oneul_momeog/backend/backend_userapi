package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class OrderWithCreateRequestDto {

    @NotNull
    private Long restaurantId;
    @NotNull
    private String roomName;
    @NotNull
    private String zipcode;
    @NotNull
    private String normalAddress;
    @NotNull
    private String specificAddress;
    @NotNull
    private List<OrderMenusDto> menus;
    @NotNull
    private Integer maxPeople;
    @NotNull
    private Integer timer;
}
