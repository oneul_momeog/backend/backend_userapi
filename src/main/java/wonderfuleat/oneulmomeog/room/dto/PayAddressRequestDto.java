package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PayAddressRequestDto {

    @NotNull
    private Long orderId;
    @NotNull
    private Long restaurantId;
    @NotNull
    private Long roomId;
    @NotNull
    private String zipcode;
    @NotNull
    private String normalAddress;
    @NotNull
    private String specificAddress;
}
