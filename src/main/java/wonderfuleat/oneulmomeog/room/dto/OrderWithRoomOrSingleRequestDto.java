package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;
import org.springframework.lang.Nullable;
import wonderfuleat.oneulmomeog.room.dto.OrderMenusDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class OrderWithRoomOrSingleRequestDto {

    @NotNull(message = "음식점 정보가 필요합니다.")
    private Long restaurantId;

    @Nullable
    private Long roomId;

    @NotNull(message = "메뉴 정보가 필요합니다.")
    private List<OrderMenusDto> menus;
}
