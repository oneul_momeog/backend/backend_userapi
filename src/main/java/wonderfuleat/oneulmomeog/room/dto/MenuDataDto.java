package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MenuDataDto {

    private Long menuId;
    private String menuName;
    private int price;
    private int quantity;
}
