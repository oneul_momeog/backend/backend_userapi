package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PayOrderRequestDto {

    @NotNull
    private Long orderId;
    @NotNull
    private Long restaurantId;
    @NotNull
    private Long roomId;
    @NotNull
    private Integer price;
}
