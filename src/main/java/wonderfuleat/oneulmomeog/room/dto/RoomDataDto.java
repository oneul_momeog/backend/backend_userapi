package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class RoomDataDto {

    private Long roomId;
    private Long restaurantId;
    private String roomName;
    private String restaurantImage;
    private String restaurantName;
    private Integer maxPeople;
    private Integer currentPeople;
    @Nullable
    private String normalAddress;
    @Nullable
    private String specificAddress;
    private LocalDateTime currentTime;
    private LocalDateTime dueTime;
}
