package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import wonderfuleat.oneulmomeog.room.domain.RoomStatus;

import java.time.LocalDateTime;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RoomReadyRequest {
    private Long restaurantId;
    private Long roomId;
    private String exMenu;
    private Integer totalPrice;
    private LocalDateTime readyTime;
    private String deliveryLocation;
    private RoomStatus status;
}

