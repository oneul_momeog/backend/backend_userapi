package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class ReadySseRequestDto {

    private Long restaurantId;
    private Long roomId;
}
