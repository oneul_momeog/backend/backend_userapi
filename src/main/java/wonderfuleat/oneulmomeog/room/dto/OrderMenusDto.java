package wonderfuleat.oneulmomeog.room.dto;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OrderMenusDto {

    private Long menuId;
    private Integer count;
}
