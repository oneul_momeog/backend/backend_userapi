package wonderfuleat.oneulmomeog.room.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class OrderDataDto {

    private Long orderId;
    private Long roomId;
    private String restaurantName;
    private String deliveryLocation;
    private List<MenuDataDto> menus;
    private int totalPrice;
    private Integer deliveryFee;
}
