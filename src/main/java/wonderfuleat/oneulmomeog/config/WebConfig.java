package wonderfuleat.oneulmomeog.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import wonderfuleat.oneulmomeog.common.interceptor.CeoLoginInterceptor;
import wonderfuleat.oneulmomeog.common.interceptor.UserLoginInterceptor;

//@PropertySources({@PropertySource("classpath:properties/env.properties")})
@Configuration
@RequiredArgsConstructor
public class WebConfig implements WebMvcConfigurer {

    private final UserLoginInterceptor userLoginInterceptor;
    private final CeoLoginInterceptor ceoLoginInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000", "http://dev.momeog.shop", "http://dev.restaurant.momeog.shop")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
                .allowedHeaders(HttpHeaders.AUTHORIZATION)
                .allowCredentials(true)
                .maxAge(3600);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userLoginInterceptor)
                .order(1)
                .addPathPatterns("/api/auth/oauth/address")
                .addPathPatterns("/api/main/**")
                .addPathPatterns("/api/reviews/**")
                .addPathPatterns("/api/restaurants/**")
                .addPathPatterns("/api/chats/**")
                .addPathPatterns("/api/orders")
                .addPathPatterns("/api/pay/**")
                .excludePathPatterns("/", "/css/**", "/*.icon", "/api/image/**", "/api/test/**");

        registry.addInterceptor(ceoLoginInterceptor)
                .order(2)
                .addPathPatterns("/api/ceo/**")
                .excludePathPatterns("/", "/css/**", "/*.icon", "/api/test/**");
    }
}
