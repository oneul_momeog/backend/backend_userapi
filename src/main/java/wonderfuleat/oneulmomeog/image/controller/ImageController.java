package wonderfuleat.oneulmomeog.image.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RestController
@Slf4j
@RequestMapping("/api/image")
public class ImageController {

    @GetMapping(produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    public InputStreamResource getImage(
            @RequestParam String imageUrl
    ) throws FileNotFoundException {
        log.info("getImage controller param : imageUrl = {}", imageUrl);
        File file = new File(imageUrl);
        FileInputStream data = new FileInputStream(file);
        return new InputStreamResource(data);
    }
}
