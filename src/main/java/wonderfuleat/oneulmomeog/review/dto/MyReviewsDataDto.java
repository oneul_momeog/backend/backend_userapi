package wonderfuleat.oneulmomeog.review.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.List;

@Getter
@AllArgsConstructor
public class MyReviewsDataDto {

    private Long reviewId;
    private Long restaurantId;
    private String restaurantName;
    private Double rating;
    private String reviewImage;
    private List<String> menus;
    @Nullable
    private String restaurantContent;
}
