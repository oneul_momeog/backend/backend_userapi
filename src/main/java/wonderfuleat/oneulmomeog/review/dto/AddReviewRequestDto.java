package wonderfuleat.oneulmomeog.review.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter @Setter
public class AddReviewRequestDto {

    private Long restaurantId;
    private Long orderId;
    private Float rating;
    private String content;
    private MultipartFile reviewImage;

}
