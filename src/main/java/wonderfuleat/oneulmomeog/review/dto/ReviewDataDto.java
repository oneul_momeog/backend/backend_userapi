package wonderfuleat.oneulmomeog.review.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ReviewDataDto {

    private Long userId;
    private String nickname;
    private Long reviewId;
    private Double rating;
    private String content;
    private String reviewImage;
    private String restaurantContent;
    private LocalDateTime createdAt;
}
