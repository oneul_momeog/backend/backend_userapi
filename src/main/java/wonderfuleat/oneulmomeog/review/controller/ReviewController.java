package wonderfuleat.oneulmomeog.review.controller;

import io.micrometer.core.instrument.util.IOUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.review.dto.MyReviewsDataDto;
import wonderfuleat.oneulmomeog.review.service.ReviewService;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/reviews")
@Slf4j
public class ReviewController {

    private final ReviewService reviewService;

    /**
     * 리뷰 작성 요청 컨트롤러
     */
    @GetMapping("/add")
    public SuccessResultWithData<Long> getOrderId(
            HttpServletRequest request,
            @RequestParam Long roomId
    ) {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        log.info("getOrderId controller param : userId = {}, roomId = {}", userId, roomId);
        Long orderId = reviewService.findOrderId(userId, roomId);
        return new SuccessResultWithData<>("주문 번호 전달 완료", orderId);
    }

    /**
     * 리뷰 저장하는 컨트롤러
     */
    @PostMapping("/add")
    public SuccessResultWithData<Map<String, Long>> addReview(
            HttpServletRequest request,
            @RequestParam Long restaurantId,
            @RequestParam Long orderId,
            @RequestParam Double rating,
            @RequestParam String content,
            @RequestParam MultipartFile reviewImage
            ) throws IOException {

        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));

        log.info("addReview controller param : userId = {}, restaurantId = {}, orderId = {}, rating = {}, " +
                "content = {}, reviewImage = {}", userId, restaurantId, orderId, rating, content, reviewImage);

        Long reviewId = reviewService.addReview(userId, restaurantId, orderId, rating, content, reviewImage);
        Map<String, Long> data = new HashMap<>();
        data.put("reviewId", reviewId);

        return new SuccessResultWithData<>("리뷰 저장이 완료되었습니다.", data);
    }

    /**
     * 리뷰 삭제 컨트롤러
     */
    @DeleteMapping("/my-review/{reviewId}")
    public SuccessResult deleteReview(HttpServletRequest request, @PathVariable Long reviewId) {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        log.info("deleteReview controller param : userId = {}, reviewId = {}", userId, reviewId);

        reviewService.deleteReview(userId, reviewId);

        return new SuccessResult("리뷰 삭제 성공");
    }

    /**
     * 리뷰 목록 컨트롤러
     */
    @GetMapping("/my-review")
    public SuccessResultWithData<List<MyReviewsDataDto>> getReviews(
            HttpServletRequest request,
            @RequestParam(defaultValue = "1") Integer page
    ) {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        log.info("getReviews controller param : userId = {}, page = {}", userId, page);

        Integer ONE_PAGE_RESULT = 10;
        Integer offset = ONE_PAGE_RESULT * (page - 1);

        List<MyReviewsDataDto> data = reviewService.getMyReviews(userId, offset, ONE_PAGE_RESULT);

        return new SuccessResultWithData<>("리뷰 목록 전달 성공", data);

    }
}

























