package wonderfuleat.oneulmomeog.review.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import wonderfuleat.oneulmomeog.common.file.FileStore;
import wonderfuleat.oneulmomeog.food.domain.Menu;
import wonderfuleat.oneulmomeog.food.repository.MenuRepository;
import wonderfuleat.oneulmomeog.review.domain.Review;
import wonderfuleat.oneulmomeog.review.dto.MyReviewsDataDto;
import wonderfuleat.oneulmomeog.review.dto.ReviewDataDto;
import wonderfuleat.oneulmomeog.review.respository.ReviewRepository;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.dto.OrderMenusDto;
import wonderfuleat.oneulmomeog.room.repository.OrdersRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class ReviewService {

    private final ReviewRepository reviewRepository;
    private final MenuRepository menuRepository;
    private final FileStore fileStore;
    private final OrdersRepository ordersRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 리뷰 작성 요청 시 orderId 를 보내주는 메서드
     */
    public Long findOrderId(Long userId, Long roomId) {
        Orders orders = ordersRepository.findByUserIdAndRoomID(userId, roomId);
        if (Objects.isNull(orders)) throw new IllegalArgumentException("해당하는 주문 정보가 없습니다.");

        return orders.getId();
    }

    /**
     * 가장 큰 점수의 리뷰를 가져오는 메서드
     */
    public ReviewDataDto getMaxRestaurantReview(Long restaurantId) {
        Review review = reviewRepository.findMaxReviewByRestaurantId(restaurantId);

        if (Objects.isNull(review)) return null;

        ReviewDataDto data = new ReviewDataDto(
                review.getUser().getId(),
                review.getUser().getNickname(),
                review.getId(),
                review.getRating(),
                review.getContent(),
                review.getReviewImage(),
                review.getRestaurantContent(),
                review.getCreatedAt()
        );

        return data;
    }

    /**
     * 해당 음식점 리뷰 목록을 가져오는 메서드
     */
    public List<ReviewDataDto> getRestaurantReviews(Long restaurantId, Integer offset, Integer limit) {
        List<Review> reviews = reviewRepository.findReviewsByRestaurantId(restaurantId, offset, limit);

        List<ReviewDataDto> data = reviews.stream().map(review ->
                new ReviewDataDto(
                        review.getUser().getId(),
                        review.getUser().getNickname(),
                        review.getId(),
                        review.getRating(),
                        review.getContent(),
                        review.getReviewImage(),
                        review.getRestaurantContent(),
                        review.getCreatedAt()
                )
        ).collect(Collectors.toList());

        return data;
    }

    /**
     * 리뷰 작성 메서드
     */
    @Transactional
    public Long addReview(Long userId, Long restaurantId, Long orderId, Double rating, String content, MultipartFile reviewImage) throws IOException {

        String filePath = fileStore.storeFile(reviewImage);
        Review review = reviewRepository.addReview(userId, restaurantId, orderId, rating, content, filePath);

        review.getRestaurant().updateRatingAndReviewCount(rating, "add");

        return review.getId();
    }

    /**
     * 리뷰 삭제 메서드
     */
    @Transactional
    public void deleteReview(Long userId, Long reviewId) {
        Review review = reviewRepository.deleteReview(userId, reviewId);

        review.getRestaurant().updateRatingAndReviewCount(review.getRating(), "delete");
    }

    /**
     * 내가 쓴 리뷰 목록 메서드
     */
    public List<MyReviewsDataDto> getMyReviews(Long userId, Integer offset, Integer limit) {

        List<Review> myReviews = reviewRepository.getMyReviews(userId, offset, limit);

        List<MyReviewsDataDto> reviewsData = myReviews.stream().map(review ->
        {
            String menus = review.getOrders().getMenus();
            try {
                List<OrderMenusDto> orderMenusDtoList = objectMapper.readValue(menus, new TypeReference<List<OrderMenusDto>>() {
                });
                log.info("menu list = {}", orderMenusDtoList.toString());

                List<Long> menuIds = orderMenusDtoList.stream()
                        .map(orderMenusDto -> orderMenusDto.getMenuId())
                        .collect(Collectors.toList());

                List<Menu> menuList = menuRepository.getMenus(menuIds);
                List<String> menuNames = menuList.stream().map(menu -> menu.getMenuName()).collect(Collectors.toList());

                return new MyReviewsDataDto(
                        review.getId(),
                        review.getRestaurant().getId(),
                        review.getRestaurant().getRestaurantName(),
                        review.getRating(),
                        review.getReviewImage(),
                        menuNames,
                        review.getRestaurantContent()
                );
            } catch (JsonProcessingException e) {
                throw new RuntimeException("json 변환 실패");
            }
        }).collect(Collectors.toList());

        return reviewsData;
    }
}













































