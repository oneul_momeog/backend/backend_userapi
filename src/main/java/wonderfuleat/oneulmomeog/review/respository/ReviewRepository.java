package wonderfuleat.oneulmomeog.review.respository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.review.domain.Review;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.user.domain.User;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class ReviewRepository {

    private final EntityManager em;

    /**
     * 해당 음식점에 가장 높은 평점의 리뷰를 가져오는 메서드
     */
    public Review findMaxReviewByRestaurantId(Long restaurantId) {
        Review review = em.createQuery("select r from Review r" +
                        " join fetch r.user" +
                        " join fetch r.restaurant" +
                        " where r.restaurant.id = :restaurantId" +
                        " order by r.rating desc", Review.class)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return review;
    }

    /**
     * 해당 음식점의 리뷰 목록을 가져오는 메서드
     */
    public List<Review> findReviewsByRestaurantId(Long restaurantId, Integer offset, Integer limit) {
        List<Review> reviews = em.createQuery("select r from Review r" +
                        " join fetch r.user u" +
                        " join r.restaurant re" +
                        " where re.id = :restaurantId" +
                        " order by r.createdAt", Review.class)
                .setParameter("restaurantId", restaurantId)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return reviews;
    }

    /**
     * 리뷰를 저장하는 메서드
     */
    public Review addReview(Long userId, Long restaurantId, Long orderId, Double rating, String content, String filePath) {
        User user = em.getReference(User.class, userId);
        Restaurant restaurant = em.getReference(Restaurant.class, restaurantId);
        Orders orders = em.getReference(Orders.class, orderId);

        Review review = new Review(user, restaurant, orders, rating, content, filePath);
        em.persist(review);
        return review;
    }

    /**
     * 리뷰 삭제 메서드
     */
    public Review deleteReview(Long userId, Long reviewId) {
        Review review = em.createQuery("select r from Review r where r.id = :reviewId and r.user.id = :userId", Review.class)
                .setParameter("reviewId", reviewId)
                .setParameter("userId", userId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        if (Objects.isNull(review)) throw new IllegalArgumentException("해당 데이터가 없습니다.");

        review.delete();
        return review;
    }

    /**
     * 내가 쓴 리뷰 목록을 가져오는 메서드
     */
    public List<Review> getMyReviews(Long userId, Integer offset, Integer limit) {
        List<Review> reviews = em.createQuery("select r from Review r" +
                        " join fetch r.orders o" +
                        " join fetch r.restaurant re" +
                        " where r.user.id = :userId and r.deleted = false", Review.class)
                .setParameter("userId", userId)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return reviews;
    }
}





























