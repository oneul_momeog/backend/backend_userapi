package wonderfuleat.oneulmomeog.food.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.food.domain.Menu;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class MenuRepository {

    private final EntityManager em;

    public List<Menu> getMenus(List<Long> menuIds) {
        List<Menu> menuList = em.createQuery("select m from Menu m" +
                        " where m.id in :menuIds", Menu.class)
                .setParameter("menuIds", menuIds)
                .getResultList();

        return menuList;
    }
}
