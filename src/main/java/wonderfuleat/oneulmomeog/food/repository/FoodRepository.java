package wonderfuleat.oneulmomeog.food.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.food.domain.FoodGroup;
import wonderfuleat.oneulmomeog.food.domain.Menu;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class FoodRepository {

    private final EntityManager em;

    /**
     * 음식 메뉴 그룹을 보여주는 메서드
     */
    public List<FoodGroup> getGroups(Long restaurantId) {
        List<FoodGroup> groups = em.createQuery("select distinct f from FoodGroup f" +
                        " join f.restaurant r" +
                        " left join fetch f.menus m" +
                        " where r.id = :restaurantId", FoodGroup.class)
                .setParameter("restaurantId", restaurantId)
                .getResultList();

        return groups;
    }

    /**
     * 그룹에 해당하는 메뉴를 보여주는 메서드
     */
    public List<Menu> getMenus(Long restaurantId, Long groupId) {
        List<Menu> menus = em.createQuery("select m from Menu m" +
                        " join m.foodGroup f" +
                        " join f.restaurant r" +
                        " where r.id = :restaurantId and f.id = :groupId", Menu.class)
                .setParameter("restaurantId", restaurantId)
                .setParameter("groupId", groupId)
                .getResultList();

        return menus;
    }



}
