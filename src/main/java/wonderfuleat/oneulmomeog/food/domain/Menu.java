package wonderfuleat.oneulmomeog.food.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import wonderfuleat.oneulmomeog.common.domain.Generate;

import javax.persistence.*;

@Entity
@DynamicInsert
@Getter
public class Menu extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menu_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private FoodGroup foodGroup;

    @Column(nullable = false)
    private String menuName;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private int price;

    @Column(nullable = false)
    private String menuImage;

    @Column(nullable = false)
    private String ingredients;

    @ColumnDefault("false")
    private Boolean soldOut;
}
