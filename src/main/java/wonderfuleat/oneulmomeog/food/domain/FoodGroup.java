package wonderfuleat.oneulmomeog.food.domain;

import lombok.Getter;
import wonderfuleat.oneulmomeog.common.domain.Generate;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
public class FoodGroup  extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupd_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @Column(nullable = false)
    private String groupName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "foodGroup")
    private List<Menu> menus = new ArrayList<>();


}
