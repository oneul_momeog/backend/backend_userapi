package wonderfuleat.oneulmomeog.food.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.food.domain.FoodGroup;
import wonderfuleat.oneulmomeog.food.domain.Menu;
import wonderfuleat.oneulmomeog.food.dto.GroupDto;
import wonderfuleat.oneulmomeog.food.dto.MenuDto;
import wonderfuleat.oneulmomeog.food.repository.FoodRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class FoodService {

    private final FoodRepository foodRepository;

    /**
     * 메뉴 그룹을 보여주는 메서드
     */
    public List<GroupDto> getGroups(Long restaurantId) {
        List<FoodGroup> groups = foodRepository.getGroups(restaurantId);

        List<GroupDto> data = groups.stream().map(foodGroup -> new GroupDto(foodGroup))
                .collect(Collectors.toList());

        return data;
    }

    /**
     * 그룹의 메뉴들을 보여주는 메서드
     */
    public List<MenuDto> getMenus(Long restaurantId, Long groupId) {
        List<Menu> menus = foodRepository.getMenus(restaurantId, groupId);

        List<MenuDto> data = menus.stream().map(menu ->
                new MenuDto(
                        menu.getId(),
                        menu.getMenuName(),
                        menu.getDescription(),
                        menu.getPrice(),
                        menu.getMenuImage(),
                        menu.getIngredients(),
                        menu.getSoldOut())
        ).collect(Collectors.toList());

        return data;
    }
}














