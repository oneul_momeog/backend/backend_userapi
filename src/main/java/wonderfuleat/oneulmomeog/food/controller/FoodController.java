package wonderfuleat.oneulmomeog.food.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.food.dto.GroupDto;
import wonderfuleat.oneulmomeog.food.dto.MenuDto;
import wonderfuleat.oneulmomeog.food.service.FoodService;

import java.util.List;

@RestController
@RequestMapping("/api/restaurants")
@RequiredArgsConstructor
@Slf4j
public class FoodController {

    private final FoodService foodService;

    /**
     * 음식점 메뉴 그룹을 가져오는 컨트롤러
     */
    @GetMapping("/{restaurantId}/groups")
    public SuccessResultWithData<List<GroupDto>> getGroups(@PathVariable Long restaurantId) {
        log.info("getGroups controller param : restaurantId = {}", restaurantId);
        List<GroupDto> data = foodService.getGroups(restaurantId);

        return new SuccessResultWithData<>("메뉴 그룹 전송 완료", data);
    }

    /**
     * 그룹의 메뉴 목록을 가져오는 컨트롤러
     */
    @GetMapping("/{restaurantId}/groups/{groupId}")
    public SuccessResultWithData<List<MenuDto>> getMenus(
            @PathVariable Long restaurantId,
            @PathVariable Long groupId
    ) {
        log.info("getMenus controller param : restaurantId = {}, groupId = {}", restaurantId, groupId);
        List<MenuDto> data = foodService.getMenus(restaurantId, groupId);

        return new SuccessResultWithData<>("메뉴 목록 전송 완료", data);
    }
}

























