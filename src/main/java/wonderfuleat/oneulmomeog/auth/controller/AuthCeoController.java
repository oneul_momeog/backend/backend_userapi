package wonderfuleat.oneulmomeog.auth.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.auth.dto.request.CeoLoginRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.request.CeoRegisterRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.response.CeoRegisterAndLoginDataResponse;
import wonderfuleat.oneulmomeog.auth.dto.response.CeoRegisterAndLoginResponse;
import wonderfuleat.oneulmomeog.auth.jwt.JwtUtil;
import wonderfuleat.oneulmomeog.auth.service.AuthCeoService;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.restaurant.domain.RestaurantAddress;
import wonderfuleat.oneulmomeog.restaurant.repository.RestaurantRepository;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/auth/ceo")
@RequiredArgsConstructor
@Slf4j
public class AuthCeoController {

    private final RestaurantRepository restaurantRepository;
    private final AuthCeoService authCeoService;
    private final JwtUtil jwtUtil;

    /**
     * 이메일 중복 확인 컨트롤러
     */
    @GetMapping("/email/check")
    public SuccessResult checkEmail(@RequestParam String email) {
        // TODO: 2022/10/25 이메일 형식 여부 확인 로직 추가

        Restaurant ceo = restaurantRepository.findByEmail(email);
        if (!Objects.isNull(ceo)) {
            throw new IllegalArgumentException("이미 가입된 이메일입니다.");
        }
        return new SuccessResult("사용 가능한 이메일입니다.");
    }

    /**
     * 회원가입 컨트롤러
     */
    @PostMapping("/restaurant/register")
    public CeoRegisterAndLoginResponse register(@Validated @RequestBody CeoRegisterRequestDto ceoRegisterRequestDto) {
        log.info("ceoRegisterRequestDto = {}", ceoRegisterRequestDto);
        Boolean isOk = ceoRegisterRequestDto.checkPassword();
        if (isOk == Boolean.FALSE) throw new IllegalArgumentException("비밀번호가 일치하지 않습니다.");

        ceoRegisterRequestDto.changePasswordByBcrypt();

        Restaurant restaurant = new Restaurant(ceoRegisterRequestDto);
        RestaurantAddress restaurantAddress = new RestaurantAddress(ceoRegisterRequestDto);
        log.info("restaurantAddress = {}", restaurantAddress);
        log.info("categories = {}", ceoRegisterRequestDto.getCategories());
        List<Category> categories = ceoRegisterRequestDto.getCategories();

        CeoRegisterAndLoginDataResponse data = authCeoService.join(restaurant, restaurantAddress, categories);
        String jwt = jwtUtil.makeJws("restaurant", data.getRestaurantId(), data.getZipcode());
        data.addJwt(jwt);

        return new CeoRegisterAndLoginResponse("회원가입이 완료되었습니다.", data);
    }

    /**
     * 로그인 컨트롤러
     */
    @PostMapping("/restaurant/login")
    public CeoRegisterAndLoginResponse login(@Validated @RequestBody CeoLoginRequestDto ceoLoginRequestDto) {
        log.info("ceoLoginRequestDto = {}", ceoLoginRequestDto);
        CeoRegisterAndLoginDataResponse data = authCeoService.login(ceoLoginRequestDto.getEmail(), ceoLoginRequestDto.getPassword());

        String jwt = jwtUtil.makeJws("restaurant", data.getRestaurantId(), data.getZipcode());
        data.addJwt(jwt);

        return new CeoRegisterAndLoginResponse("로그인 성공", data);
    }

}
