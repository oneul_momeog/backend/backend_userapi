package wonderfuleat.oneulmomeog.auth.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoTokenResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoUserInfoResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.naver.NaverTokenResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.naver.NaverUserInfoResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.request.AddAddressRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.response.UserFirstLoginDataResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.response.UserFirstLoginResponseDto;
import wonderfuleat.oneulmomeog.auth.jwt.JwtUtil;
import wonderfuleat.oneulmomeog.auth.service.AuthUserService;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.user.domain.LocalStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping("/api/auth/oauth")
@RequiredArgsConstructor
@Slf4j
public class AuthUserController {

    private final AuthUserService authUserService;
    private final JwtUtil jwtUtil;

    /**
     * 카카오 로그인
     */
    @GetMapping("/login/kakao")
    public UserFirstLoginResponseDto kakaoLogin(@RequestParam String code) {

        log.info("code first = {}", code);

        // 토큰 받기
        KakaoTokenResponseDto result = authUserService.getKakaoToken(code);
        log.info("result = {}", result.toString());

        // 사용자 정보 받기
        KakaoUserInfoResponseDto userInfo = authUserService.getKakaoUserInfo(result.getAccess_token());
        log.info("userInfo = {}", userInfo);

        // 최초 로그인인 경우 사용자 저장
        LocalStatus local = LocalStatus.KAKAO;
        UserFirstLoginDataResponseDto userSaveData = authUserService.saveByUserId(userInfo.getId(), userInfo.getKakao_account().getProfile().getNickname(), local);
        log.info("userSaveData: userId = {}, userNickname = {}, userZipcode = {}", userSaveData.getUserId(), userSaveData.getNickname(), userSaveData.getZipcode());

        // 토큰 생성
        String jwt = jwtUtil.makeJws("user", userSaveData.getUserId(), userSaveData.getNickname(), userSaveData.getZipcode());
        userSaveData.addJwt(jwt);

        // 최초 로그인인 경우
        if (userSaveData.getZipcode() == null || userSaveData.getZipcode().isEmpty()) {
            return new UserFirstLoginResponseDto("주소입력이 필요합니다.", Boolean.FALSE, userSaveData);
        }

        return new UserFirstLoginResponseDto("로그인 성공 완료했습니다.", Boolean.TRUE, userSaveData);
    }

    /**
     * 네이버 로그인
     */
    @GetMapping("/login/naver")
    public UserFirstLoginResponseDto naverLogin(@RequestParam String code) {
        log.info("code = {}", code);

        // 토큰 받기
        NaverTokenResponseDto result = authUserService.getNaverToken(code);
        log.info("result = {}", result.toString());

        // 사용자 정보 받기
        NaverUserInfoResponseDto userInfo = authUserService.getNaverUserInfo(result.getAccess_token());
        log.info("userInfo = {}", userInfo);

        // 최초 로그인인 경우 사용자 저장
        LocalStatus local = LocalStatus.NAVER;
        UserFirstLoginDataResponseDto userSaveData = authUserService.saveByUserId(userInfo.getResponse().getId(), userInfo.getResponse().getNickname(), local, userInfo.getResponse().getEmail());
        log.info("userSaveData = {}", userSaveData);

        // 토큰 생성
        String jwt = jwtUtil.makeJws("user", userSaveData.getUserId(), userSaveData.getNickname(), userSaveData.getZipcode());
        userSaveData.addJwt(jwt);

        // 최초 로그인인 경우
        if (userSaveData.getZipcode() == null || userSaveData.getZipcode().isEmpty()) {
            return new UserFirstLoginResponseDto("주소입력이 필요합니다.", Boolean.FALSE, userSaveData);
        }

        return new UserFirstLoginResponseDto("로그인 성공 완료했습니다.", Boolean.TRUE, userSaveData);
    }

    /**
     * 주소 입력
     */
    @PostMapping("/address")
    public SuccessResultWithData<String> addAddress(
            @Validated @RequestBody AddAddressRequestDto addAddressRequestDto,
            HttpServletRequest request
    ) {
        Long id = Long.valueOf(Objects.toString(request.getAttribute("id")));
        String nickname = Objects.toString(request.getAttribute("nickname"));

        log.info("token user id = {}", id);
        log.info("addAddressRequestDto = {}", addAddressRequestDto);

        authUserService.saveAddress(id, addAddressRequestDto.getZipcode(), addAddressRequestDto.getNormalAddress(), addAddressRequestDto.getSpecificAddress());

        String jwt = jwtUtil.makeJws("user", id, nickname, addAddressRequestDto.getZipcode());
        return new SuccessResultWithData<>("주소 저장 성공", jwt);
    }

    /**
     * token 에러 처리
     */
    @GetMapping("/token-error")
    public SuccessResult tokenError() {
        log.info("token에 문제가 있음");
        return new SuccessResult("token 에러");
    }
}





























