package wonderfuleat.oneulmomeog.auth.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoTokenRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoTokenResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoUserInfoResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.naver.NaverTokenRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.naver.NaverTokenResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.naver.NaverUserInfoResponseDto;
import wonderfuleat.oneulmomeog.auth.dto.response.UserFirstLoginDataResponseDto;
import wonderfuleat.oneulmomeog.user.domain.LocalStatus;
import wonderfuleat.oneulmomeog.user.domain.User;
import wonderfuleat.oneulmomeog.user.domain.UserAddress;
import wonderfuleat.oneulmomeog.user.repository.UserAddressRepository;
import wonderfuleat.oneulmomeog.user.repository.UserRepository;

import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class AuthUserService {

    private final UserRepository userRepository;
    private final UserAddressRepository userAddressRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${kakao.client.id}")
    String KAKAO_CLIENT_ID;

    @Value("${kakao.redirect.url}")
    String KAKAO_REDIRECT_URL;

    @Value("${naver.client.id}")
    String NAVER_CLIENT_ID;

    @Value("${naver.client.secret}")
    String NAVER_CLIENT_SECRET;

    @Value("${naver.state}")
    String NAVER_STATE;

    @Value("${naver.redirect.url}")
    String NAVER_REDIRECT_URL;


    // TODO: 2022/11/27 webclientUtil을 이용해서 리팩터링하기
    /**
     * 인증토큰 받아오는 함수(카카오)
     */
    public KakaoTokenResponseDto getKakaoToken(String code) {
        KakaoTokenRequestDto dto = new KakaoTokenRequestDto(KAKAO_CLIENT_ID, KAKAO_REDIRECT_URL, code);

        MultiValueMap valueMap = new LinkedMultiValueMap<String, Object>();
        Map<String, Object> fieldMap = objectMapper.convertValue(dto, new TypeReference<Map<String, Object>>() {});
        valueMap.setAll(fieldMap);


        WebClient webClient = WebClient.builder()
                .baseUrl("https://kauth.kakao.com")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();

        Mono<KakaoTokenResponseDto> response = webClient.post()
                .uri("/oauth/token")
                .bodyValue(valueMap)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(KakaoTokenResponseDto.class);

        log.info("Mono Token response = {}", response);
        KakaoTokenResponseDto result = response.block();
        log.info("kakao login getToken result = {}", result);

        return result;
    }

    /**
     * 사용자 정보를 받아오는 함수(카카오)
     */
    public KakaoUserInfoResponseDto getKakaoUserInfo(String accessToken) {
        log.info("accessToken = {}", accessToken);
        WebClient webClient = WebClient.builder()
                .baseUrl("https://kapi.kakao.com")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .build();

        Mono<KakaoUserInfoResponseDto> response = webClient.get()
                .uri("/v2/user/me")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(KakaoUserInfoResponseDto.class);

        log.info("Mono Info response = {}", response);
        KakaoUserInfoResponseDto result = response.block();
        log.info("kako login user Info result = {}", result);

        return result;
    }

    /**
     * 인증토큰을 받아오는 함수(네이버)
     */
    public NaverTokenResponseDto getNaverToken(String code) {
        NaverTokenRequestDto dto = new NaverTokenRequestDto("authorization_code", NAVER_CLIENT_ID, NAVER_CLIENT_SECRET, code, NAVER_STATE);

        MultiValueMap valueMap = new LinkedMultiValueMap<String, Object>();
        Map<String, Object> fieldMap = objectMapper.convertValue(dto, new TypeReference<Map<String, Object>>() {});
        valueMap.setAll(fieldMap);


        WebClient webClient = WebClient.builder()
                .baseUrl("https://nid.naver.com")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();

        Mono<NaverTokenResponseDto> response = webClient.post()
                .uri("/oauth2.0/token")
                .bodyValue(valueMap)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(NaverTokenResponseDto.class);

        log.info("Mono Token response = {}", response);
        NaverTokenResponseDto result = response.block();
        log.info("naver login getToken result = {}", result);

        return result;
    }

    /**
     * 사용자 정보를 받아오는 함수(네이버)
     */
    public NaverUserInfoResponseDto getNaverUserInfo(String accessToken) {
        log.info("accessToken = {}", accessToken);
        WebClient webClient = WebClient.builder()
                .baseUrl("https://openapi.naver.com")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                .build();

        Mono<NaverUserInfoResponseDto> response = webClient.get()
                .uri("/v1/nid/me")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(NaverUserInfoResponseDto.class);

        log.info("Mono Info response = {}", response);
        NaverUserInfoResponseDto result = response.block();
        log.info("naver login user Info result = {}", result);

        return result;
    }

    /**
     * 사용자 저장 함수
     */
    @Transactional
    public UserFirstLoginDataResponseDto saveByUserId(String userId, String nickname, LocalStatus localStatus) {
        User foundedUser = userRepository.findByUserId(userId);

        // 최초 로그인인 경우
        if (Objects.isNull(foundedUser)) {
            log.info("first login : foundedUser = {}", foundedUser);
            User user = new User(userId, nickname, localStatus);
            userRepository.save(user);

            return new UserFirstLoginDataResponseDto(user.getId(), user.getNickname(), null, null);
        }

        // 주소 입력이 안된 경우
        if (foundedUser.getUserAddressList().isEmpty() || foundedUser.getUserAddressList() == null) {
            log.info("no address : foundedUser = {} with no address", foundedUser);
            return new UserFirstLoginDataResponseDto(foundedUser.getId(), foundedUser.getNickname(), null, null);
        }

        log.info("with address : foundedUser = {} with address = {}", foundedUser, foundedUser.getUserAddressList().get(0));
        return new UserFirstLoginDataResponseDto(
                foundedUser.getId(),
                foundedUser.getNickname(),
                foundedUser.getUserAddressList().get(0).getZipcode(),
                foundedUser.getUserAddressList().get(0).getNormalAddress());
    }

    @Transactional
    public UserFirstLoginDataResponseDto saveByUserId(String userId, String nickname, LocalStatus localStatus, String email) {
        User foundedUser = userRepository.findByUserId(userId);

        // 최초 로그인인 경우
        if (Objects.isNull(foundedUser)) {
            User user = new User(userId, nickname, localStatus, email);
            userRepository.save(user);

            return new UserFirstLoginDataResponseDto(user.getId(), user.getNickname(), null, null);
        }

        // 주소 입력이 안된 경우
        if (foundedUser.getUserAddressList().isEmpty() || foundedUser.getUserAddressList() == null) {
            return new UserFirstLoginDataResponseDto(foundedUser.getId(), foundedUser.getNickname(), null, null);
        }

        return new UserFirstLoginDataResponseDto(
                foundedUser.getId(),
                foundedUser.getNickname(),
                foundedUser.getUserAddressList().get(0).getZipcode(),
                foundedUser.getUserAddressList().get(0).getNormalAddress());
    }

    /**
     * 주소 입력
     */
    @Transactional
    public void saveAddress(Long id, String zipcode, String normalAddress, String specificAddress) {

        User foundedUser = userRepository.findById(id);
        log.info("foundedUser = {}", foundedUser.getId());

        // basic 주소로 저장
        UserAddress userAddress = new UserAddress(foundedUser, zipcode, normalAddress, specificAddress);
        userAddressRepository.save(userAddress);
    }
}






















