package wonderfuleat.oneulmomeog.auth.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.auth.dto.response.CeoRegisterAndLoginDataResponse;
import wonderfuleat.oneulmomeog.auth.jwt.JwtUtil;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.restaurant.domain.RestaurantAddress;
import wonderfuleat.oneulmomeog.restaurant.domain.RestaurantCategory;
import wonderfuleat.oneulmomeog.restaurant.repository.RestaurantCategoryRepository;
import wonderfuleat.oneulmomeog.restaurant.repository.RestaurantRepository;

import java.util.List;
import java.util.Objects;

@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class AuthCeoService {

    private final RestaurantRepository restaurantRepository;
    private final RestaurantCategoryRepository restaurantCategoryRepository;

    @Transactional
    public CeoRegisterAndLoginDataResponse join(Restaurant restaurant, RestaurantAddress restaurantAddress, List<Category> categories) {
        Restaurant foundRestaurant = restaurantRepository.findByEmail(restaurant.getEmail());
        if (!Objects.isNull(foundRestaurant)) throw new IllegalArgumentException("이미 가입된 이메일입니다.");

        log.info("restaurantAddress = {}", restaurantAddress);
        restaurant.addRestaurantAddress(restaurantAddress);
        restaurantRepository.save(restaurant);

        for (Category category : categories) {
            RestaurantCategory restaurantCategory = new RestaurantCategory(restaurant, category);
            log.info("restaurantCategory = {}", restaurantCategory.getCategory());
            restaurantCategoryRepository.save(restaurantCategory);
        }

        return new CeoRegisterAndLoginDataResponse(
                restaurant.getId(),
                restaurant.getRestaurantName(),
                restaurantAddress.getZipcode(),
                restaurantAddress.getBranch(),
                restaurant.getOpen());
    }

    public CeoRegisterAndLoginDataResponse login(String email, String password) {
        Restaurant foundRestaurant = restaurantRepository.findByEmail(email);
        if (Objects.isNull(foundRestaurant)) throw new IllegalArgumentException("이메일과 일치하는 회원이 없습니다.");

        if (!BCrypt.checkpw(password, foundRestaurant.getPassword())) {
            throw new IllegalArgumentException("비밀번호가 일치하지 않습니다.");
        }

        return new CeoRegisterAndLoginDataResponse(
                foundRestaurant.getId(),
                foundRestaurant.getRestaurantName(),
                foundRestaurant.getRestaurantAddress().getZipcode(),
                foundRestaurant.getRestaurantAddress().getBranch(),
                foundRestaurant.getOpen());
    }

}
