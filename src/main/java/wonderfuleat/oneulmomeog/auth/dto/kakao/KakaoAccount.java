package wonderfuleat.oneulmomeog.auth.dto.kakao;

import lombok.Getter;

@Getter
public class KakaoAccount {
    private Profile profile;
}
