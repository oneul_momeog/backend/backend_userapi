package wonderfuleat.oneulmomeog.auth.dto.kakao;

import lombok.Getter;

@Getter
public class Profile {
    private String nickname;
}
