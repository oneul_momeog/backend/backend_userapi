package wonderfuleat.oneulmomeog.auth.dto.request;

import lombok.Getter;
import lombok.ToString;
import org.mindrot.jbcrypt.BCrypt;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@ToString
public class CeoRegisterRequestDto {

    @Email
    private String email;

    @NotNull
    private String password;

    @NotNull
    private String passwordCheck;

    @NotNull
    private String restaurantName;

    @NotNull
    private List<Category> categories;

    @NotNull
    private String zipcode;

    @NotNull
    private String normalAddress;

    @NotNull
    private String specificAddress;

    @NotNull
    private String branch;

    // 비즈니스 메서드
    public void changePasswordByBcrypt() {
        String hashedPw = BCrypt.hashpw(password, BCrypt.gensalt());
        this.password = hashedPw;
    }

    public Boolean checkPassword() {
        return getPassword().equals(getPasswordCheck());
    }
}
