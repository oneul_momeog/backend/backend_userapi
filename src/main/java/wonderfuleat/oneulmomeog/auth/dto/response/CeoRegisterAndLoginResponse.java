package wonderfuleat.oneulmomeog.auth.dto.response;

import lombok.Getter;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;

@Getter
public class CeoRegisterAndLoginResponse extends SuccessResult {

    private CeoRegisterAndLoginDataResponse data;

    public CeoRegisterAndLoginResponse(String message, CeoRegisterAndLoginDataResponse ceoRegisterAndLoginDataResponse) {
        super(message);
        this.data = ceoRegisterAndLoginDataResponse;
    }
}
