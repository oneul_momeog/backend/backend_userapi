package wonderfuleat.oneulmomeog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@PropertySources({@PropertySource("classpath:properties/env.properties")})
@EnableScheduling
@SpringBootApplication
@EnableJpaAuditing
public class OneulmomeogApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneulmomeogApplication.class, args);
	}

}
