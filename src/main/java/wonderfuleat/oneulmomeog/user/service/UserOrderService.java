package wonderfuleat.oneulmomeog.user.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.food.domain.Menu;
import wonderfuleat.oneulmomeog.food.repository.MenuRepository;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.dto.OrderMenusDto;
import wonderfuleat.oneulmomeog.user.dto.OrdersDto;
import wonderfuleat.oneulmomeog.user.dto.UserOrdersDto;
import wonderfuleat.oneulmomeog.user.repository.UserOrderRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class UserOrderService {

    private final UserOrderRepository userOrderRepository;
    private final MenuRepository menuRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    public List<OrdersDto> getUserOrders(Long userId, Integer offset, Integer limit) {
        List<Orders> userOrders = userOrderRepository.getUserOrders(userId, offset, limit);

        log.info("userOrders = {}", userOrders.toString());

        List<OrdersDto> ordersDtoList = userOrders.stream().map(order -> {

            String menus = order.getMenus();
            List<String> menuNames = new ArrayList<>();
            try {
                List<OrderMenusDto> orderMenusDtoList = objectMapper.readValue(menus, new TypeReference<List<OrderMenusDto>>() {
                });
                log.info("menu list = {}", orderMenusDtoList.toString());

                List<Long> menuIds = orderMenusDtoList.stream()
                        .map(orderMenusDto -> orderMenusDto.getMenuId())
                        .collect(Collectors.toList());

                List<Menu> menuList = menuRepository.getMenus(menuIds);
                menuNames = menuList.stream().map(menu -> menu.getMenuName()).collect(Collectors.toList());

            } catch (RuntimeException | JsonProcessingException e) {
                throw new RuntimeException(e);
            } finally {
                OrdersDto ordersDto = new OrdersDto(
                        order.getRoom().getRestaurant().getId(),
                        order.getRoom().getRestaurant().getRestaurantName(),
                        order.getRoom().getRestaurant().getRestaurantAddress().getBranch(),
                        order.getId(),
                        menuNames,
                        order.getCreatedAt()
                );
                return ordersDto;
            }
        }).collect(Collectors.toList());

        return ordersDtoList;
    }

}
