package wonderfuleat.oneulmomeog.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@AllArgsConstructor
public class OrdersDto {

    private Long restaurantId;
    private String restaurantName;
    private String branch;
    private Long orderId;
    private List<String> menus;
    private LocalDateTime orderDate;
}
