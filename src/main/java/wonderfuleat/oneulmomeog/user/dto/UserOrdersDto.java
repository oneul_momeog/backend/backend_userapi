package wonderfuleat.oneulmomeog.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class UserOrdersDto {

    private List<OrdersDto> orders;
}
