package wonderfuleat.oneulmomeog.user.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.user.dto.OrdersDto;
import wonderfuleat.oneulmomeog.user.dto.UserOrdersDto;
import wonderfuleat.oneulmomeog.user.service.UserOrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/orders")
@Slf4j
@RequiredArgsConstructor
public class UserOrderController {

    private final UserOrderService userOrderService;

    @GetMapping
    public SuccessResultWithData<UserOrdersDto> getUserOrders(
            HttpServletRequest request,
            @RequestParam(defaultValue = "1") Integer page
    ) {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));
        log.info("getUserOrders controller param : userId = {}, page = {}", userId, page);

        int offset = page - 1;
        int LIMIT_VAL = 10;

        List<OrdersDto> data = userOrderService.getUserOrders(userId, offset, LIMIT_VAL);

        return new SuccessResultWithData<>("사용자 주문 목록 전달 성공", new UserOrdersDto(data));
    }
}
