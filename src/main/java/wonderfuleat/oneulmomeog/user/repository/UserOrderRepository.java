package wonderfuleat.oneulmomeog.user.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.room.domain.Orders;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserOrderRepository {

    private final EntityManager em;

    // 사용자 주문 목록
    public List<Orders> getUserOrders(Long userId, Integer offset, Integer limit) {
        List<Orders> orders = em.createQuery("select o from Orders o" +
                        " join fetch o.room r" +
                        " join fetch r.restaurant re" +
                        " join fetch re.restaurantAddress" +
                        " where o.user.id = :userId" +
                        " order by o.createdAt desc", Orders.class)
                .setParameter("userId", userId)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
        return orders;
    }
}
