package wonderfuleat.oneulmomeog.user.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.user.domain.UserAddress;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class UserAddressRepository {

    private final EntityManager em;

    public void save(UserAddress userAddress) {
        em.persist(userAddress);
    }

    public UserAddress findByUserId(Long id) {
        return em.createQuery("select ad from UserAddress ad where ad.user.id = :id", UserAddress.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }
}
