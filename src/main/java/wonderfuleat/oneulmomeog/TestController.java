package wonderfuleat.oneulmomeog;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoTokenRequestDto;
import wonderfuleat.oneulmomeog.auth.dto.kakao.KakaoTokenResponseDto;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/test")
@Slf4j
public class TestController {

    @GetMapping("/test")
    public String test() {
        log.info("test controller");

        return "Test Ok";
    }
}
