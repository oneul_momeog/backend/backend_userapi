package wonderfuleat.oneulmomeog.timer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.room.domain.Orders;
import wonderfuleat.oneulmomeog.room.repository.RoomRepository;

import java.util.TimerTask;

public interface ScheduledJob {

    public void transactionExecute();
}
