package wonderfuleat.oneulmomeog.restaurant.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.room.domain.Room;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class RestaurantRepository {

    private final EntityManager em;

    public Restaurant findByRestaurantId(Long restaurantId) {
        Restaurant restaurant = em.createQuery("select r from Restaurant r join fetch r.restaurantAddress" +
                        " where r.id = :restaurantId", Restaurant.class)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return restaurant;
    }


    public Restaurant findByEmail(String email) {
        return em.createQuery("select r from Restaurant r where r.email = :email", Restaurant.class)
                .setParameter("email", email)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    public void save(Restaurant restaurant) {
        em.persist(restaurant);
    }


    /**
     * 음식점 목록을 보여주는 메서드
     */
    public List<Restaurant> findRestaurants(String zipcode, Integer offset, Integer limit) {
        List<Restaurant> restaurants = em.createQuery("select r from Restaurant r" +
                        " join fetch r.restaurantAddress ra" +
                        " where ra.zipcode like :zipcode", Restaurant.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return restaurants;
    }

    public List<Restaurant> findRestaurants(String zipcode, Integer offset, Integer limit, List<Long> categories) {
        List<Restaurant> restaurants = em.createQuery("select r from Restaurant r" +
                        " join fetch r.restaurantAddress ra" +
                        " where ra.zipcode like :zipcode" +
                        " and r.id in (" +
                        "select distinct rc from RestaurantCategory rc where rc.category.categoryId in :categories)", Restaurant.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("categories", categories)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return restaurants;
    }

    /**
     * 검색창을 이용한 음식점 목록을 보여주는 메서드
     */
    public List<Restaurant> searchRestaurants(String zipcode, Integer offset, Integer limit, String search) {
        List<Restaurant> restaurants = em.createQuery("select r from Restaurant r" +
                        " join fetch r.restaurantAddress ra" +
                        " where ra.zipcode like :zipcode and r.open = true" +
                        " and (r.restaurantName like :search" +
                        " or r.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryName like :search))", Restaurant.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("search", "%" + search + "%")
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return restaurants;
    }

    public List<Restaurant> searchRestaurants(String zipcode, Integer offset, Integer limit, String search, List<Long> categories) {
        List<Restaurant> restaurants = em.createQuery("select r from Restaurant r" +
                        " join fetch r.restaurantAddress ra" +
                        " where ra.zipcode like :zipcode and r.open = true" +
                        " and (r.restaurantName like :search" +
                        " or r.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryName like :search))" +
                        " and r.id in (select distinct rc.restaurant.id from RestaurantCategory rc where rc.category.categoryId in :categories)", Restaurant.class)
                .setParameter("zipcode", zipcode.substring(0, 3) + "%")
                .setParameter("search", "%" + search + "%")
                .setParameter("categories", categories)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        return restaurants;
    }
}



























