package wonderfuleat.oneulmomeog.restaurant.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.restaurant.domain.RestaurantAddress;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class RestaurantAddressRepository {

    private final EntityManager em;

    public void save(RestaurantAddress restaurantAddress) {
        this.em.persist(restaurantAddress);
    }
}
