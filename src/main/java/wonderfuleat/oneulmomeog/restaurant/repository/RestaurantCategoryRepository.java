package wonderfuleat.oneulmomeog.restaurant.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.restaurant.domain.RestaurantCategory;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class RestaurantCategoryRepository {

    private final EntityManager em;

    public void save(RestaurantCategory restaurantCategory) {
        em.persist(restaurantCategory);
    }

}
