package wonderfuleat.oneulmomeog.restaurant.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class CategoryRepository {

    private final EntityManager em;

    public void dbInit() {
        Category category1 = new Category("족발, 보쌈");
        Category category2 = new Category("찜, 탕, 찌개");
        Category category3 = new Category("돈까스, 회, 일식");
        Category category4 = new Category("피자");
        Category category5 = new Category("고기, 구이");
        Category category6 = new Category("양식");
        Category category7 = new Category("치킨");
        Category category8 = new Category("중식");
        Category category9 = new Category("아시안");
        Category category10 = new Category("백반, 죽, 국수");
        Category category11 = new Category("도시락");
        Category category12 = new Category("분식");
        Category category13 = new Category("페스트푸드");
        this.em.persist(category1);
        this.em.persist(category2);
        this.em.persist(category3);
        this.em.persist(category4);
        this.em.persist(category5);
        this.em.persist(category6);
        this.em.persist(category7);
        this.em.persist(category8);
        this.em.persist(category9);
        this.em.persist(category10);
        this.em.persist(category11);
        this.em.persist(category12);
        this.em.persist(category13);
    }

    public Category findById(Long id) {
        return em.find(Category.class, id);
    }

    public List<Category> findAll() {
        List<Category> result = em.createQuery("select c from Category c", Category.class)
                .getResultList();

        return result;
    }
}





















