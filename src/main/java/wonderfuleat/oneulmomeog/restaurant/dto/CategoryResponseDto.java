package wonderfuleat.oneulmomeog.restaurant.dto;

import lombok.Getter;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;

import java.util.List;

@Getter
public class CategoryResponseDto extends SuccessResult {

    private List<Category> data;

    public CategoryResponseDto(String message, List<Category> data) {
        super(message);
        this.data = data;
    }
}
