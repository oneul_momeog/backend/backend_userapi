package wonderfuleat.oneulmomeog.restaurant.dto;

import lombok.Getter;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResult;

import java.util.List;

@Getter
public class RestaurantResponseDto extends SuccessResult {
    private List<RestaurantDataDto> data;

    public RestaurantResponseDto(String message, List<RestaurantDataDto> data) {
        super(message);
        this.data = data;
    }
}
