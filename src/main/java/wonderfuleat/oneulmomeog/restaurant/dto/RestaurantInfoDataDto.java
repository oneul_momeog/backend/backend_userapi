package wonderfuleat.oneulmomeog.restaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RestaurantInfoDataDto {

    private Long restaurantId;
    private String restaurantImage;
    private String restaurantName;
    private String branch;
    private String notice;
    private String event;
    private Double meanRating;
}
