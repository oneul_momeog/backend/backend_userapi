package wonderfuleat.oneulmomeog.restaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CeoManageDataDto {

    private Long restaurantId;
    private String notice;
    private String event;
    private String ingredientOrigin;
}
