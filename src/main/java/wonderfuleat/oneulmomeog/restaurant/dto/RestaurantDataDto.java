package wonderfuleat.oneulmomeog.restaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RestaurantDataDto {

    private Long restaurantId;
    private String restaurantName;
    private String restaurantImage;
    private Double meanRating;
    private String specificAddress;
    private String branch;
}
