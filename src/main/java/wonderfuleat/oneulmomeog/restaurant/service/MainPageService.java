package wonderfuleat.oneulmomeog.restaurant.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.restaurant.dto.RestaurantDataDto;
import wonderfuleat.oneulmomeog.restaurant.repository.RestaurantRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class MainPageService {

    private final RestaurantRepository restaurantRepository;

    /**
     * 음식점 목록을 보여주는 메서드
     */
    public List<RestaurantDataDto> getRestaurants(String zipcode, Integer offset, Integer limit, List<Long> categories) {
        log.info("get Restaurant service");

        List<Restaurant> restaurants = new ArrayList<>();
        
        if (categories.get(0) == 0) {
            restaurants = restaurantRepository.findRestaurants(zipcode, offset, limit);
        } else {
            restaurants = restaurantRepository.findRestaurants(zipcode, offset, limit, categories);
        }

        List<RestaurantDataDto> data = restaurantToDataDtos(restaurants);
        return data;
    }

    /**
     * 검색창을 이용한 음식점 목록을 보여주는 메서드
     */
    public List<RestaurantDataDto> searchRestaurants(String zipcode, Integer offset, Integer limit, List<Long> categories, String search) {
        log.info("get Restaurant service by search");

        List<Restaurant> restaurants = new ArrayList<>();

        if (categories.get(0) == 0) {
            restaurants = restaurantRepository.searchRestaurants(zipcode, offset, limit, search);
        } else {
            restaurants = restaurantRepository.searchRestaurants(zipcode, offset, limit, search, categories);
        }

        List<RestaurantDataDto> data = restaurantToDataDtos(restaurants);
        return data;

    }


    private List<RestaurantDataDto> restaurantToDataDtos(List<Restaurant> restaurants) {
        List<RestaurantDataDto> result = restaurants.stream().map(restaurant ->
                new RestaurantDataDto(
                        restaurant.getId(),
                        restaurant.getRestaurantName(),
                        restaurant.getRestaurantImage(),
                        restaurant.getMeanRating(),
                        restaurant.getRestaurantAddress().getSpecificAddress(),
                        restaurant.getRestaurantAddress().getBranch())
        ).collect(Collectors.toList());

        return result;
    }

}



























