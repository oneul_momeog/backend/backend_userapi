package wonderfuleat.oneulmomeog.restaurant.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wonderfuleat.oneulmomeog.restaurant.domain.Restaurant;
import wonderfuleat.oneulmomeog.restaurant.dto.RestaurantInfoDataDto;
import wonderfuleat.oneulmomeog.restaurant.repository.RestaurantRepository;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class RestaurantService {

    private final RestaurantRepository restaurantRepository;

    /**
     * 음식점 정보를 보내주는 메서드
     */
    public RestaurantInfoDataDto getRestaurantInfo(Long restaurantId) {
        Restaurant restaurant = restaurantRepository.findByRestaurantId(restaurantId);

        if (Objects.isNull(restaurant)) throw new IllegalArgumentException("해당 음식점이 없습니다.");

        RestaurantInfoDataDto data = new RestaurantInfoDataDto(
                restaurant.getId(),
                restaurant.getRestaurantImage(),
                restaurant.getRestaurantName(),
                restaurant.getRestaurantAddress().getBranch(),
                restaurant.getNotice(),
                restaurant.getEvent(),
                restaurant.getMeanRating()
        );

        return data;
    }

}
