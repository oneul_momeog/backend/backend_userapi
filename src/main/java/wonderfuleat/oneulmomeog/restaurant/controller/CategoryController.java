package wonderfuleat.oneulmomeog.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.restaurant.domain.Category;
import wonderfuleat.oneulmomeog.restaurant.dto.CategoryResponseDto;
import wonderfuleat.oneulmomeog.restaurant.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/main")
@RequiredArgsConstructor
@Slf4j
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("/category")
    public CategoryResponseDto getAllCategory() {
        List<Category> data = categoryService.getAllCategory();

        log.info("categories = {}", data.toString());

        CategoryResponseDto response = new CategoryResponseDto("카테고리 전달 성공", data);

        return response;
    }

}
