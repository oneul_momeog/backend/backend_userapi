package wonderfuleat.oneulmomeog.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.restaurant.dto.CeoManageDataDto;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ceo")
@Slf4j
public class CeoManageController {
//
//    @GetMapping("/{restaurantId}/management")
//    public SuccessResultWithData<CeoManageDataDto> getRestaurantManageInfo(
//            @PathVariable Long restaurantId,
//            HttpServletRequest request
//    ) {
//        Long logRestaurantId = Long.valueOf(Objects.toString(request.getAttribute("id")));
//        if (!logRestaurantId.equals(restaurantId)) {
//            throw new IllegalArgumentException("음식점 권한이 없습니다.");
//        }
//        log.info("getRestaurantManageInfo controller param : restaurantId = {}", restaurantId);
//
//
//    }
}
