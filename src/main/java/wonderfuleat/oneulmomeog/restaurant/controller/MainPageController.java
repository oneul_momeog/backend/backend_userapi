package wonderfuleat.oneulmomeog.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import wonderfuleat.oneulmomeog.restaurant.dto.RestaurantDataDto;
import wonderfuleat.oneulmomeog.restaurant.dto.RestaurantResponseDto;
import wonderfuleat.oneulmomeog.restaurant.service.MainPageService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/main/restaurants")
@Slf4j
public class MainPageController {

    private final MainPageService mainPageService;

    private final Integer ONE_PAGE_ROOMS_NUM = 6;

    @GetMapping
    public RestaurantResponseDto findRestaurants(
            HttpServletRequest request,
            @RequestParam(value = "category", defaultValue = "0") List<Long> categories,
            @RequestParam(defaultValue = "1") Integer page
            ) {
        String zipcode = String.valueOf(request.getAttribute("zipcode"));
        log.info("params : zipcode = {}, category = {}, page = {}", zipcode, categories.toString(), page);

        int offset = ONE_PAGE_ROOMS_NUM * (page - 1);

        List<RestaurantDataDto> data = mainPageService.getRestaurants(zipcode, offset, ONE_PAGE_ROOMS_NUM, categories);

        RestaurantResponseDto response = new RestaurantResponseDto("음식점 목록 응답 완료", data);
        return response;
    }

    @GetMapping("/search")
    public RestaurantResponseDto searchRestaurants(
            HttpServletRequest request,
            @RequestParam(value = "category", defaultValue = "0") List<Long> categories,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam String search
    ) {
        String zipcode = String.valueOf(request.getAttribute("zipcode"));
        log.info("params : zipcode = {}, category = {}, page = {}, search = {}", zipcode, categories.toString(), page, search);

        int offset = ONE_PAGE_ROOMS_NUM * (page - 1);

        List<RestaurantDataDto> data = mainPageService.searchRestaurants(zipcode, offset, ONE_PAGE_ROOMS_NUM, categories, search);

        RestaurantResponseDto response = new RestaurantResponseDto("음식점 검색 목록 응답 완료", data);
        return response;
    }

}
