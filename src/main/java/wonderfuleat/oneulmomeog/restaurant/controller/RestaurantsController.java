package wonderfuleat.oneulmomeog.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import wonderfuleat.oneulmomeog.common.responsedto.SuccessResultWithData;
import wonderfuleat.oneulmomeog.restaurant.dto.RestaurantInfoDataDto;
import wonderfuleat.oneulmomeog.restaurant.service.RestaurantService;
import wonderfuleat.oneulmomeog.review.dto.ReviewDataDto;
import wonderfuleat.oneulmomeog.review.service.ReviewService;
import wonderfuleat.oneulmomeog.room.dto.RoomDataDto;
import wonderfuleat.oneulmomeog.room.service.RoomService;

import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/restaurants")
public class RestaurantsController {

    private final RestaurantService restaurantService;
    private final ReviewService reviewService;
    private final RoomService roomService;

    /**
     * 음식점 정보를 응답하는 컨트롤러
     */
    @GetMapping("/{restaurantId}")
    public SuccessResultWithData<RestaurantInfoDataDto> getRestaurantInfo(@PathVariable Long restaurantId) {
        log.info("getRestaurantInfo controller param = {}", restaurantId);
        RestaurantInfoDataDto data = restaurantService.getRestaurantInfo(restaurantId);

        return new SuccessResultWithData<>("음식점 정보 응답 성공", data);
    }

    /**
     * 선택한 방 정보를 응답하는 컨트롤러
     */
    @GetMapping("/{restaurantId}/room/default")
    public SuccessResultWithData<RoomDataDto> getSelectedRoomInfo(
            @PathVariable Long restaurantId,
            @RequestParam Long roomId
    ) {
        log.info("getSelectedRoomInfo controller");
        RoomDataDto data = roomService.getRoomInfo(roomId, restaurantId);

        return new SuccessResultWithData<>("룸 정보 전달 성공", data);
    }

    /**
     * 해당 음식점의 공동구매 방 목록
     */
    @GetMapping("/{restaurantId}/room/list")
    public SuccessResultWithData<List<RoomDataDto>> getRoomListForRestaurant(
            @PathVariable Long restaurantId,
            @RequestParam(defaultValue = "1") Integer page
    ) {
        log.info("getRoomListForRestaurant controller param : restaurantId = {}, page = {}", restaurantId, page );

        Integer ONE_PAGE_ROOM_NUM = 10;
        Integer offset = ONE_PAGE_ROOM_NUM * (page - 1);

        List<RoomDataDto> data = roomService.getRoomListByRestaurantId(restaurantId, offset, ONE_PAGE_ROOM_NUM);

        return new SuccessResultWithData<>("룸 목록 전달 성공", data);
    }

    /**
     * 음식점의 최고 점수 리뷰를 가져오는 컨트롤러
     */
    @GetMapping("/{restaurantId}/review")
    public SuccessResultWithData<ReviewDataDto> getRestaurantReview(@PathVariable Long restaurantId) {
        log.info("getRestaurantReview controller param = {}", restaurantId);
        ReviewDataDto data = reviewService.getMaxRestaurantReview(restaurantId);

        if (Objects.isNull(data)) return new SuccessResultWithData<>("리뷰 정보가 없습니다.", null);

        return new SuccessResultWithData<>("리뷰 정보 응답 성공", data);
    }

    /**
     * 해당 음식점의 리뷰 더보기 클릭
     */
    @GetMapping("/{restaurantId}/review/list")
    public SuccessResultWithData<List<ReviewDataDto>> getRestaurantReviews(
            @PathVariable Long restaurantId,
            @RequestParam(defaultValue = "1") Integer page
    ) {
        log.info("getRestaurantReviews controller param : restaurantId = {}, page = {}", restaurantId, page);
        Integer ONE_PAGE_REVIEW_NUM = 10;
        Integer offset = ONE_PAGE_REVIEW_NUM * (page - 1);

        List<ReviewDataDto> data = reviewService.getRestaurantReviews(restaurantId, offset, ONE_PAGE_REVIEW_NUM);

        return new SuccessResultWithData<>("리뷰 목록 전달 성공", data);
    }
}




























