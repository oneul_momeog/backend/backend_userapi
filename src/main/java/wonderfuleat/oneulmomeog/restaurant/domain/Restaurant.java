package wonderfuleat.oneulmomeog.restaurant.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import wonderfuleat.oneulmomeog.auth.dto.request.CeoRegisterRequestDto;
import wonderfuleat.oneulmomeog.common.domain.Generate;
import wonderfuleat.oneulmomeog.food.domain.FoodGroup;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@DynamicInsert
public class Restaurant extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restaurant_id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "restaurant_address_id")
    private RestaurantAddress restaurantAddress;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String restaurantName;

    private String restaurantImage;

    @ColumnDefault("false")
    private Boolean open;

    private String notice;
    private String event;
    private String ingredientsOrigin;
    @ColumnDefault("4000")
    private Integer deliveryFee;

    @Column(columnDefinition = "decimal(2,1) default 0")
    private Double meanRating;

    @Column(columnDefinition = "int default 0")
    private Integer reviewCount;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "restaurant")
    private List<FoodGroup> foodGroups;

    // 생성자 메서드
    protected Restaurant() {}

    public Restaurant(CeoRegisterRequestDto ceoRegisterRequestDto) {
        this.email = ceoRegisterRequestDto.getEmail();
        this.password = ceoRegisterRequestDto.getPassword();
        this.restaurantName = ceoRegisterRequestDto.getRestaurantName();
    }

    // 연관관계 메핑 메서드
    public void addRestaurantAddress(RestaurantAddress restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    // 비즈니스 메서드
    public void updateRatingAndReviewCount(Double rating, String type) {
        if (type.equals("add")) {
            Integer newReviewCount = this.reviewCount + 1;
            this.meanRating = Math.ceil(((this.meanRating * this.reviewCount) + rating) * 10 / newReviewCount) / 10;
            this.reviewCount = newReviewCount;
        } else { // delete
            Integer newReviewCount = this.reviewCount - 1;
            this.meanRating = Math.ceil(((this.meanRating * this.reviewCount) - rating) * 10 / newReviewCount) / 10f;
            this.reviewCount = newReviewCount;
        }
    }

}
