package wonderfuleat.oneulmomeog.restaurant.domain;

import lombok.Getter;
import wonderfuleat.oneulmomeog.auth.dto.request.CeoRegisterRequestDto;

import javax.persistence.*;

@Entity
@Getter
public class RestaurantAddress {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restaurant_address_id")
    private Long id;

    @Column(nullable = false)
    private String zipcode;

    @Column(nullable = false)
    private String normalAddress;

    @Column(nullable = false)
    private String specificAddress;

    @Column(nullable = false)
    private String branch;

    protected RestaurantAddress() {}

    public RestaurantAddress(CeoRegisterRequestDto ceoRegisterRequestDto) {
        this.zipcode = ceoRegisterRequestDto.getZipcode();
        this.normalAddress = ceoRegisterRequestDto.getNormalAddress();
        this.specificAddress = ceoRegisterRequestDto.getSpecificAddress();
        this.branch = ceoRegisterRequestDto.getBranch();
    }
}
