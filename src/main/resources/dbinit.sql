-- 음식점 주소
insert into restaurant_address(zipcode, normal_address, specific_address, branch) values('10325', '위시티4로 46', '스타타워1층', '식사점');
insert into restaurant_address(zipcode, normal_address, specific_address, branch) values('10326', '위시티4로 47', '주상복함1층', '식사점');

-- 음식점
insert into restaurant(restaurant_address_id, email, password, restaurant_name, restaurant_image, notice, event) values(1, 'restaurant1@naver.com', 'restaurant1', 'restaurant1', 'restaurant1', 'notice1', 'event1');
insert into restaurant(restaurant_address_id, email, password, restaurant_name, restaurant_image, notice, event) values(2, 'restaurant2@naver.com', 'restaurant2', 'restaurant2', 'restaurant2', 'notice2', 'event2');

-- 음식점 카테고리
insert into restaurant_category(restaurant_id, category_id) values(1, 1);
insert into restaurant_category(restaurant_id, category_id) values(2, 2);

-- 음식 그룹
insert into food_group(restaurant_id, group_name) values(1, '족발');
insert into food_group(restaurant_id, group_name) values(1, '보쌈');
insert into food_group(restaurant_id, group_name) values(2, '찜');
insert into food_group(restaurant_id, group_name) values(2, '탕');

-- 요리 메뉴
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(1, '마약 족발', '맛있어요1', 15000, 'menu1', '돼지고기, 마늘');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(1, '불맛 족발', '맛있어요2', 20000, 'menu2', '돼지고기, 마늘, 청양고추');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(2, '마약 보쌈', '맛있어요3', 10000, 'menu3', '보쌈, 마늘');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(2, '불맛 보쌈', '맛있어요4', 13000, 'menu4', '보쌈, 마늘, 청양고추');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(3, '해물찜', '맛있어요5', 19000, 'menu5', '생선, 고춧가루, 문어');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(3, '소꼬리찜', '맛있어요6', 30000, 'menu6', '한우, 꼬리, 간장');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(4, '매운탕', '맛있어요7', 15000, 'menu7', '생선 대가리');
insert into menu(group_id, menu_name, description, price, menu_image, ingredients) values(4, '연포탕', '맛있어요8', 25000, 'menu8', '문어, 전복, 생선');

-- 사용자
insert into users(user_id, nickname, local_status) values('111111', 'user1', 'KAKAO');
insert into users(user_id, nickname, local_status) values('222222', 'user2', 'NAVER');

-- 사용자 주소
insert into user_address(user_id, zipcode, normal_address, specific_address, basic) values(1, '10323', '위시티4로 45', '1203호', true);
insert into user_address(user_id, zipcode, normal_address, specific_address, basic) values(1, '10330', '위시티4로 50', '103호', false);
insert into user_address(user_id, zipcode, normal_address, specific_address, basic) values(2, '10331', '위시티4로 51', '104호', true);
insert into user_address(user_id, zipcode, normal_address, specific_address, basic) values(2, '10332', '위시티4로 52', '1503호', false);

-- 주문 방 배달 위치
insert into room_address(zipcode, normal_address, specific_address) values('10323', '위시티4로 45', '1층');
insert into room_address(zipcode, normal_address, specific_address) values('10339', '위시티4로 53', '1층');

-- 음식 주문 방
insert into room(restaurant_id, room_address_id, room_name, ex_menu, max_people, current_people, total_price, timer) values(1, 1, '내집에서 공동 구매', '마약 족발', 3, 2, 25000, 10);
insert into room(restaurant_id, room_address_id, room_name, ex_menu, max_people, current_people, total_price, timer) values(2, 2, '공동 위치에서 공동 구매', '해물찜', 4, 1, 19000, 8);

-- 사용자 방 다대다 매핑 (주문 테이블)
insert into orders(user_id, room_id, menu, total_price, status) values(1, 1, '{menuId: 1, count: 1}', 15000, 'PAY');
insert into orders(user_id, room_id, menu, total_price, status) values(2, 1, '{menuId: 3, count: 1}', 10000, 'PAY');
insert into orders(user_id, room_id, menu, total_price, status) values(2, 2, '{menuId: 5, count: 1}', 19000, 'PAY');


















